@extends('layouts.app')
@section('content')
    <section class="section parallax-container breadcrumbs-wrap" data-parallax-img="images/bg-breadcrumbs-1-1920x726.jpg">
        <div class="parallax-content breadcrumbs-custom context-dark">
            <div class="container">
                <h3 class="breadcrumbs-custom-title">Login and Register</h3>
                <ul class="breadcrumbs-custom-path">
                    <li><a href="/">Home</a></li>
                    <li><a href="">Features</a></li>
                    <li class="active">Login and Register</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Section Login/register-->
    <section class="section section-variant-1 bg-gray-100">
        <div class="container">
            <div class="row row-50 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-6">
                    <div class="card-login-register {{isset(request()->ref_nickname) ? 'active': ''}}" id="card-l-r">
                        <div class="card-top-panel">
                            <div class="card-top-panel-left">
                                <h5 class="card-title card-title-login">Login</h5>
                                <h5 class="card-title card-title-register">Register</h5>
                            </div>
                            <div class="card-top-panel-right"><span class="card-subtitle"><span class="card-subtitle-login">Register now</span><span class="card-subtitle-register">Sign in</span></span>
                                <button class="card-toggle {{isset(request()->ref_nickname) ? 'active': ''}}" data-custom-toggle="#card-l-r"><span class="card-toggle-circle"></span></button>
                            </div>
                        </div>
                        <div class="card-form card-form-login">
                            <form action="{{route('login')}}" method="post" class="rd-form login-form">
                                @csrf
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-name-1">Login</label>
                                    <input class="form-input" id="form-login-name-1" type="text" name="email" data-constraints="@Email @Required">
                                    @if($errors->has('email'))
                                        <spna style="color: red" id="email" >{{$errors->get('email')[0]}}</spna>
                                    @endif
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-password-1">Password</label>
                                    <input class="form-input" id="form-login-password-1" type="password" name="password" data-constraints="@Required">
                                    @if($errors->has('password'))
                                        <spna style="color: red" id="password">{{$errors->get('password')[0]}}</spna>
                                    @endif
                                </div>
                                <button class="button button-lg button-primary button-block" type="submit">Sign in</button>
                            </form>
                            <div class="group-sm group-sm-justify group-middle">
                                <a class="button button-google button-icon button-icon-left button-round" href="{{route('google-login')}}">
                                    <span class="icon fa fa-google-plus"></span><span>Google+</span>
                                </a>
                                <a class="button button-facebook button-icon button-icon-left button-round" href="{{route('facebook-login')}}">
                                    <span class="icon fa fa-facebook"></span><span>Facebook</span>
                                </a></div>
                        </div>
                        <div class="card-form card-form-register">
                            <form id="register-form" method="post" action="{{route('register')}}">
                                @csrf
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-name-2">First Name</label>
                                    <input class="form-input" id="form-login-name-2" type="text" name="first_name" data-constraints="@Required">
                                    <span style="color: red" id="first_name"></span>
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-name-3">Last Name</label>
                                    <input class="form-input" id="form-login-name-3" type="text" name="last_name" data-constraints="@Required">
                                    <span style="color: red" id="last_name"></span>
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="form-register-email">E-mail</label>
                                    <input class="form-input" id="form-register-email" type="email" name="email" data-constraints="@Email @Required">
                                    <span style="color: red" id="email"></span>
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-password-2">Password</label>
                                    <input class="form-input" id="form-login-password-2" type="password" name="password" data-constraints="@Required">
                                    <span style="color: red" id="password"></span>
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="form-login-password-3">Repeat Password</label>
                                    <input class="form-input" id="form-login-password-3" type="password" name="password_confirmation" data-constraints="@Required">
                                    <span style="color: red" id="password_confirmation"></span>
                                </div>
                                <div class="form-wrap {{!isset(request()->ref_nickname) ? 'd-none' : ''}}">
                                    <label class="form-label" for="form-reg-nick-name">Referral Name</label>
                                    <input class="form-input" readonly id="form-reg-nick-name" name="ref_nickname" value="{{isset(request()->ref_nickname) ? request()->ref_nickname : ''}}">
                                    <span style="color: red"  id="ref_nickname"></span>
                                </div>
                                <button class="button button-lg button-primary button-block">Create new account</button>
                            </form>
                            <div class="group-sm group-sm-justify group-middle">
                                <a class="button button-google button-icon button-icon-left button-round" href="{{route('google-login')}}">
                                    <span class="icon fa fa-google-plus"></span><span>Google+</span></a>
                                <a class="button button-facebook button-icon button-icon-left button-round" href="{{route('facebook-login')}}">
                                    <span class="icon fa fa-facebook"></span><span>Facebook</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
