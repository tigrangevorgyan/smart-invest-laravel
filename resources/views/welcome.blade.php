<!DOCTYPE html>
<html class="wide wow-animation" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,500i,600,900%7CRoboto:400,900">
        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" id="main-styles-link">
        <!-- Styles -->
        <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3)
            ;clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
    </head>
    <body>
    <div class="ie-panel"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader">
        <div class="preloader-body">
            <div class="preloader-item"></div>
        </div>
    </div>
    <div class="page">
       @include('layouts.header')
       @include('includes.slider')

        <section class="section section-md bg-gray-100">
            <div class="container">
                <div class="row row-50">
                    <div class="col-lg-8">
                        <div class="main-component">
                            <!-- Heading Component-->
                            <article class="heading-component">
                                <div class="heading-component-inner">
                                    <h5 class="heading-component-title">Popular news
                                    </h5><a class="button button-xs button-gray-outline" href="{{ route('news.index') }}">All news</a>
                                </div>
                            </article>
                            <div class="row row-30">
                                @foreach($news as $singleNews)
                                    <div class="col-md-6">
                                        <article class="post-future"><a class="post-future-figure" href="/news/{{ $singleNews->id }}"><img src="images/static_icons/{{ $singleNews->attachment->path }}" alt="" width="368" height="287"/></a>
                                            <div class="post-future-main">
                                                <h4 class="post-future-title"><a href="/news/{{ $singleNews->id }}">{{ $singleNews->title }}</a></h4>
                                                <div class="post-future-meta">
                                                    <div class="post-future-time"><span class="icon mdi mdi-clock"></span>
                                                        <time datetime="2020">{{ $singleNews->created_at }}</time>
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="post-future-text">
                                                    <p>{{ $singleNews->description }}</p>
                                                </div>
                                                <div class="post-future-footer group-flex group-flex-xs"><a class="button button-gray-outline" href="/news/{{ $singleNews->id }}">Read more</a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Aside Block-->
                    <div class="col-lg-4">
                        <aside class="aside-components">
                            <div class="aside-component">
                                <!-- Heading Component-->
                                <article class="heading-component">
                                    <div class="heading-component-inner">
                                        <h5 class="heading-component-title">In the spotlight
                                        </h5><a class="button button-xs button-gray-outline" href="{{ route("news.index") }}">All news</a>
                                    </div>
                                </article>
                                <!-- List Post Classic-->
                                <div class="list-post-classic">
                                    <!-- Post Classic-->
                                    <article class="post-classic">
                                        <div class="post-classic-aside"><a class="post-classic-figure" href="blog-post.html"><img src="images/blog-element-1-94x94.jpg" alt="" width="94" height="94"/></a></div>
                                        <div class="post-classic-main">
                                            <p class="post-classic-title"><a href="blog-post.html">Raheem Sterling turns the tide for Manchester</a></p>
                                            <div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
                                                <time datetime="2020">April 15, 2020</time>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Post Classic-->
                                    <article class="post-classic">
                                        <div class="post-classic-aside"><a class="post-classic-figure" href="blog-post.html"><img src="images/blog-element-2-94x94.jpg" alt="" width="94" height="94"/></a></div>
                                        <div class="post-classic-main">
                                            <p class="post-classic-title"><a href="blog-post.html">Prem in 90 seconds: Chelsea's crisis is over!</a></p>
                                            <div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
                                                <time datetime="2020">April 15, 2020</time>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Post Classic-->
                                    <article class="post-classic">
                                        <div class="post-classic-aside"><a class="post-classic-figure" href="blog-post.html"><img src="images/blog-element-3-94x94.jpg" alt="" width="94" height="94"/></a></div>
                                        <div class="post-classic-main">
                                            <p class="post-classic-title"><a href="blog-post.html">Good vibes back at struggling Schalke</a></p>
                                            <div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
                                                <time datetime="2020">April 15, 2020</time>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Post Classic-->
                                    <article class="post-classic">
                                        <div class="post-classic-aside"><a class="post-classic-figure" href="blog-post.html"><img src="images/blog-element-4-94x94.jpg" alt="" width="94" height="94"/></a></div>
                                        <div class="post-classic-main">
                                            <p class="post-classic-title"><a href="blog-post.html">Liverpool in desperate need of backup players</a></p>
                                            <div class="post-classic-time"><span class="icon mdi mdi-clock"></span>
                                                <time datetime="2020">April 15, 2020</time>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            @include('includes.plans_table', [
                                'plans' => $plans
                            ])
                            <div class="aside-component">
                                <!-- Heading Component-->
                                <article class="heading-component">
                                    <div class="heading-component-inner">
                                        <h5 class="heading-component-title">Follow us
                                        </h5>
                                    </div>
                                </article>
                                <!-- Buttons Media-->
                                <div class="group-sm group-flex"><a class="button-media button-media-facebook" href="index.html#">
                                        <h4 class="button-media-title">50k</h4>
                                        <p class="button-media-action">Like<span class="icon material-icons-add_circle_outline icon-sm"></span></p><span class="button-media-icon fa-facebook"></span></a><a class="button-media button-media-twitter" href="index.html#">
                                        <h4 class="button-media-title">120k</h4>
                                        <p class="button-media-action">Follow<span class="icon material-icons-add_circle_outline icon-sm"></span></p><span class="button-media-icon fa-twitter"></span></a><a class="button-media button-media-google" href="index.html#">
                                        <h4 class="button-media-title">15k</h4>
                                        <p class="button-media-action">Follow<span class="icon material-icons-add_circle_outline icon-sm"></span></p><span class="button-media-icon fa-google"></span></a><a class="button-media button-media-instagram" href="index.html#">
                                        <h4 class="button-media-title">85k</h4>
                                        <p class="button-media-action">Follow<span class="icon material-icons-add_circle_outline icon-sm"></span></p><span class="button-media-icon fa-instagram"></span></a></div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
       @include('layouts.footer')
    </div>
    </body>
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0],j=d.createElement(s),
                dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})
        (window,document,'script','dataLayer','GTM-P9FT69');</script>
    <script src="{{ asset('js/core.min.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
</html>
