<div class="modal fade" id="actionNotCompletedModal" role="dialog">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="page-wrapper">
            <div class="custom-modal">
                <div class="danger danger-animation icon-top"><i class="fa fa-times"></i></div>
                <div class="danger border-bottom"></div>
                <div class="content">
                    <p id="actionNotCompletedModalText" class="message-type"></p>
                </div>
            </div>
        </div>
    </div>
</div>
