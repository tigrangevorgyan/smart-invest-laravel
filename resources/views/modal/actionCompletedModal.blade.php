<div class="modal fade" id="actionCompletedModal" role="dialog">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="page-wrapper">
            <div class="custom-modal">
                <div class="succes succes-animation icon-top"><i class="fa fa-check"></i></div>
                <div class="succes border-bottom"></div>
                <div class="content">
                    <p id="actionCompletedModalText" class="message-type"></p>
                </div>
            </div>
        </div>
    </div>
</div>


