@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Frequently asked questions
                    </h5>
                </div>
            </article>

            <div class="contact-list">
                <dl>
                    <dt>1. How much should I invest?</dt>
                    <dd>The game has many subscriptions(teams). You can find them <a href="{{ route("plan") }}">here.</a></dd>
                    <dt>2. Why you pay us more then we invest?</dt>
                    <dd>The game use your investments and transfer them to another platform.
                        That platform is already in cloud mining market many years.
                        Game pays you percents from earned money from cloud mining platform.
                    </dd>
                    <dt>3. Can I take my money back?</dt>
                    <dd>Contact to our support team, and they will do their best to solve any problem.</dd>
                    <dt>4. What is <a href="/payable-partners">"payable partners"</a> section for?</dt>
                    <dd>Unblocking this section, you can find more then 15 websites, where you can earn real money.
                        All websites located in that section are already checked. They pay!
                    </dd>
                    <dt>5. What if I have forgotten my transaction code?</dt>
                    <dd>You can go to <a href="/payment-settings">settings page</a> and request for a new transaction code.</dd>
                    <dt>5. What if I've forgotten my transaction code and at the same time can not access to my email?</dt>
                    <dd>Contact to our support team, and they will do their best to solve any problem.</dd>
                </dl>
            </div>
        </div>
    </section>
@endsection
