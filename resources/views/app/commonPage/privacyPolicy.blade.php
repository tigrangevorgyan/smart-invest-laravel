@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Privacy Policy
                    </h5>
                </div>
            </article>

            <div class="contact-list">
                <dl>
                    <dd>We don`t share any personal(secured) information with third part person.</dd>
                    <br>
                    <dd>Any amount on your "For withdraw" balance is yours, and there is no way to lose it.</dd>
                    <br>
                    <dd>Any amount on your "For shopping" balance is yours, and you can use it only for making new subscriptions.</dd>
                </dl>
            </div>
        </div>
    </section>
@endsection
