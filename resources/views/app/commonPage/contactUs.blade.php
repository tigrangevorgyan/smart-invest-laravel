@extends('layouts.app')

@section('content')
    @include('modal.actionCompletedModal')
    @include('modal.actionNotCompletedModal')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Contact Us
                    </h5>
                </div>
            </article>

            <div class="contact-list">
                <div class="p-4">
                    <div class="d-flex row">
                        <div class="col-md-6 text-dark">You can contact us by "teamstarshine1@gmail.com" email or submit request here</div>
                        <div class="col-md-6">
                            <form id="send-contact-us-email" action="{{route('contactAsStore')}}" method="post">
                                @csrf
                                <div class="form-wrap">
                                    <label class="form-label" for="contact-us-email">Email</label>
                                    <input required class="form-input" id="contact-us-email" type="text" name="email" data-constraints="@Email @Required">
                                </div>
                                <div class="form-wrap">
                                    <label class="form-label" for="contact-us-subject">Subject</label>
                                    <input required class="form-input" id="contact-us-subject" type="text" name="subject" data-constraints="@Required">
                                </div>
                                <div class="form-wrap">
                                    <textarea required placeholder="Message" class="form-input" id="contact-us-subject" type="text" name="text" data-constraints="@Required"></textarea>
                                </div>
                                <button class="button button-lg button-primary button-block" type="submit">Send</button>
                            </form>
                        </div>
                    </div>
                    <div class="text-dark">Our support team will response you during 4 hours.</div>
                </div>
            </div>
        </div>
    </section>
@endsection
