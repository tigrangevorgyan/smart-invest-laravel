@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">About Us
                    </h5>
                </div>
            </article>

            <div class="contact-list">
                <dl>
                    <dt>Introduction</dt>
                    <dd>This is an investing game where you can subscribe to your favorite sport clubs,
                        and get your daily income until your subscriptions' finish.</dd>
                    <dd>All subscriptions lifecycle is 180 days started from subscribe day.</dd>
                    <br>
                    <dt>Referral system</dt>
                    <dd>
                        StarShine has referral system.
                        This means you can earn also from your referrals actions.
                        We don`t have any limitations for referrals count.
                        So feel free to be in top positions and get famous inside our big team.
                    </dd>
                    <dd>More about this you find <a href="/rules">here</a>.</dd>
                    <br>
                    <dt>Todo-s</dt>
                    <dd>In near future you will have ability to connect your subscribed teams to the same club results in reality.
                        It means if your favorite club wins its' next game, then you will receive bonus from StarsShine team.
                    </dd>
                    <dd>There will be various types of bonuses in future.</dd>
                    <br>
                    <dt>Brief</dt>
                    <dd>Game uses your investment to increase power of our cloud mining system.</dd>
                    <dd>Game pays you more then you invest from our cloud mining income.</dd>
                </dl>
            </div>
        </div>
    </section>
@endsection
