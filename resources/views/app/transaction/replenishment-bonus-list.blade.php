@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Replenishment Bonuses
                    </h5>
                </div>
            </article>
            <div class="product-cart">
                <div class="table-custom-responsive slider">
                    <table class="table-custom table-product">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Amount</th>
                            <th>Type</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($replenishmentBonuses as $bonus)
                        <tr>
                            <td>
                                <div class="product-cart-price"><span>{{ $bonus['id'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$ {{ $bonus['amount'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $bonus['type_display_name'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $bonus['created_at'] }}</span></div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
