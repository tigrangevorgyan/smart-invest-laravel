@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Replenishments
                    </h5>
                </div>
            </article>
            <div class="product-cart">
                <div class="table-custom-responsive slider">
                    <table class="table-custom table-product">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Amount</th>
                            <th>System Number</th>
                            <th>System Name</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($replenishments as $replenishment)
                        <tr>
                            <td>
                                <div class="product-cart-price"><span>{{ $replenishment['id'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$ {{ $replenishment['amount'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $replenishment['system_number'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $replenishment['system_name'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $replenishment['created_at'] }}</span></div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
