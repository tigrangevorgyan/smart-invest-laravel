@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Withdraws
                    </h5>
                </div>
            </article>
            <div class="product-cart">
                <div class="table-custom-responsive slider">
                    <table class="table-custom table-product">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Amount</th>
                            <th>System Number</th>
                            <th>System Name</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($withdraws as $withdraw)
                        <tr>
                            <td>
                                <div class="product-cart-price"><span>{{ $withdraw['id'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$ {{ $withdraw['amount'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $withdraw['system_number'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $withdraw['system_name'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $withdraw['status_display_name'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>{{ $withdraw['created_at'] }}</span></div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
