@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
    <div class="container">
        <div class="row row-50">
            <div class="col-sm-12">
                <!-- Heading Component-->
                <article class="heading-component">
                    <div class="heading-component-inner">
                        <h5 class="heading-component-title">Payment methods for replenishment
                        </h5>
                    </div>
                </article>
                <div class="row row-30">
                    <div class="col-md-6 col-lg-4">
                        <!-- Product - Grid build-->
                        <article class="product">
                            <header class="product-header">
                                <!-- Badge-->
                                <div class="badge badge-red">new<span class="icon material-icons-whatshot"></span>
                                </div>
                                <div class="product-figure"><img src="images/partners/payeer_2.png" alt="" width="200px" height="200px"/></div>
                            </header>
                            <footer class="product-content">
                                <h6 class="product-title"><a href="product-page.html">Payeer</a></h6>
                                <ul class="product-rating">
                                    <li><span class="material-icons-star"></span></li>
                                    <li><span class="material-icons-star"></span></li>
                                    <li><span class="material-icons-star"></span></li>
                                    <li><span class="material-icons-star"></span></li>
                                    <li><span class="material-icons-star"></span></li>
                                </ul>
                                <div>
                                    <span class="product-price-new">Minimum 0.1$</span>
                                </div>
                                <div class="product-price">
                                    <span class="product-price-new">Commission 1%</span>
                                </div>
                                <br>
                                <form action="/replenishment" method="post">
                                    @csrf
                                    <label for="payeerReplenishment"></label>
                                    <input type="text" id="payeerReplenishment" required name="amount" placeholder="enter amount">
                                    <input type="submit" value="Add funds">
                                </form>
                            </footer>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@endsection
