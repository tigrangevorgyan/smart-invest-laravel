@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container text-center">
            <div class="row row-50 text-left">
                <div class="col-md-12">
                    <div class="row row-50">
                        <div class="col-lg-12">
                            <!-- Heading Component-->
                            <article class="heading-component">
                                <div class="heading-component-inner">
                                    <h5 class="heading-component-title">Profile
                                    </h5>
                                </div>
                            </article>
                            <form action="user/{{$user->id}}" id="user-profile-update" method="post">
                                {{ method_field('PUT') }}
                                @csrf
                                <div class="">
                                    <div class="row row-10 row-narrow">
                                        <div class="col-md-6">
                                            <label class="form-label" for="profile-first-name">First Name</label>
                                            <input class="form-input" id="profile-first-name" type="text"
                                                   name="first_name"
                                                   value="{{$user->first_name ? $user->first_name : ''}}"
                                                   data-constraints="@Required">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="profile-last-name">Last Name</label>
                                            <input class="form-input" id="profile-last-name" type="text"
                                                   name="last_name"
                                                   value="{{$user->last_name ? $user->last_name : ''}}"
                                                   data-constraints="@Required">
                                        </div>
                                        <div class="col-md-6 mt-0" style="height: 12px">
                                            <span style="color: red" id="first_name"></span>
                                        </div>
                                        <div class="col-md-6" style="height: 12px">
                                            <span style="color: red" id="last_name"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label" for="profile-email">E-mail</label>
                                            <input class="form-input" id="profile-email"
                                                   value="{{$user->email ? $user->email : ''}}"
                                                   type="email" disabled name="email" data-constraints="@Email @Required">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="profile-password">Password</label>
                                            <input class="form-input" value="" id="profile-password" type="password"
                                                   name="password">

                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="profile-old-password">Old Password</label>
                                            <input class="form-input" value="" id="profile-old-password" type="password"
                                                   name="oldPassword">
                                        </div>
                                        <div class="col-md-6 " style="height: 22px">
                                            <span style="color: red" id="password"></span>
                                        </div>
                                        <div class="col-md-6" style="height: 22px">
                                            <span style="color: red" id="changePassword"></span>
                                        </div>

                                        <div class="col-md-6 m-auto">
                                            <button type="submit" class="button button-lg button-primary button-block">
                                                Update profile
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
