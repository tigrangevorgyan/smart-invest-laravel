@extends('layouts.app')

@section('content')
    @include('modal.actionCompletedModal')
    @include('modal.actionNotCompletedModal')
    <section class="section section-md bg-gray-100">
        <div class="container text-center">
            <div class="row row-50 text-left">
                <div class="col-lg-12">
                    <!-- Heading Component-->
                    <article class="heading-component">
                        <div class="heading-component-inner">
                            <h5 class="heading-component-title">Payment Methods
                            </h5>
                        </div>
                    </article>
                    <div class="card-shop">
                        <ul class="list-md form-wrap">
                            <li>
                                <div class="radio-panel">
                                    <label class="radio-inline radio-inline-lg active">
                                        <input name="input-radio" checked value="radio-2" type="radio">peyeer
                                    </label>
                                    <div class="radio-panel-content">
                                        <form action="user/{{$user->id}}/payment-system-info" method="post">
                                            {{ method_field('PUT') }}
                                            @csrf
                                            <div class="form-wrap">
                                                <div class="row row-10 row-narrow">
                                                    <div class="col-md-6">
                                                        <div class="form-wrap">
                                                            <label class="form-label" for="payeer-account">Peyeer
                                                                Account</label>
                                                            <input class="form-input" id="payeer-account"
                                                                   value="{{$user->payeerSystemInfo->account ?? ''}}"
                                                                   type="text" name="account"
                                                                   data-constraints="@Required">
                                                        </div>
                                                        @if($errors->has('account'))
                                                            <span
                                                                style="color: red">{{ $errors->first('account')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-wrap">
                                                            <label class="form-label" for="transaction_code">Transaction
                                                                code</label>
                                                            <input class="form-input" id="transaction_code" type="text"
                                                                   name="transaction_code" data-constraints="@Required">
                                                        </div>
                                                        @if($errors->has('transaction_code'))
                                                            <span
                                                                style="color: red">{{ $errors->first('transaction_code')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <button class="button button-sm button-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="type" value="1">
                                        </form>

                                    </div>
                                </div>
                            </li>
                            <p class="info-text-container">"If you dont remember your transaction code click send button and to your
                                mail we will send the code"</p>
                            <form id="send-new-transaction" action="user/{{$user->id}}/new-transaction-code" method="post">
                                @csrf
                                <div class="form-button text-center">
                                    <button class="button button-sm button-gray-outline" type="submit"
                                            aria-label="Send">
                                        <svg class="svg-color-primary svg-sizing-35" x="0px" y="0px"
                                             width="72px" height="72px" viewbox="0 0 72 72">
                                            <path
                                                d="M36.002,0c-0.41,0-0.701,0.184-0.931,0.332c-0.23,0.149-0.4,0.303-0.4,0.303l-9.251,8.18H11.58 c-1.236,0-1.99,0.702-2.318,1.358c-0.329,0.658-0.326,1.3-0.326,1.3v11.928l-8.962,7.936V66c0,0.015-0.038,1.479,0.694,2.972 C1.402,70.471,3.006,72,5.973,72h30.03h30.022c2.967,0,4.571-1.53,5.306-3.028c0.736-1.499,0.702-2.985,0.702-2.985V31.338 l-8.964-7.936V11.475c0,0,0.004-0.643-0.324-1.3c-0.329-0.658-1.092-1.358-2.328-1.358H46.575l-9.251-8.18 c0,0-0.161-0.154-0.391-0.303C36.703,0.184,36.412,0,36.002,0z M36.002,3.325c0.49,0,0.665,0.184,0.665,0.184l6,5.306h-6.665 h-6.665l6-5.306C35.337,3.51,35.512,3.325,36.002,3.325z M12.081,11.977h23.92H59.92v9.754v2.121v14.816L48.511,48.762 l-10.078-8.911c0,0-0.307-0.279-0.747-0.548s-1.022-0.562-1.684-0.562c-0.662,0-1.245,0.292-1.686,0.562 c-0.439,0.268-0.747,0.548-0.747,0.548l-10.078,8.911L12.082,38.668V23.852v-2.121v-9.754H12.081z M8.934,26.867v9.015 l-5.091-4.507L8.934,26.867z M63.068,26.867l5.091,4.509l-5.091,4.507V26.867z M69.031,34.44v31.559 c0,0.328-0.103,0.52-0.162,0.771L50.685,50.684L69.031,34.44z M2.971,34.448l18.348,16.235L3.133,66.77 c-0.059-0.251-0.162-0.439-0.162-0.769C2.971,66.001,2.971,34.448,2.971,34.448z M36.002,41.956c0.264,0,0.437,0.057,0.546,0.104 c0.108,0.047,0.119,0.059,0.119,0.059l30.147,26.675c-0.3,0.054-0.79,0.207-0.79,0.207H36.002H5.98H5.972 c-0.003,0-0.488-0.154-0.784-0.207l30.149-26.675c0,0,0.002-0.011,0.109-0.059C35.555,42.013,35.738,41.956,36.002,41.956z"></path>
                                        </svg>
                                    </button>
                                </div>
                            </form>
                        </ul>
                    </div>
                </div>
            </div>
    </section>
@endsection
