@extends('layouts.app')

@section('content')
    @include('modal.actionCompletedModal')
    @include('modal.actionNotCompletedModal')
    <section class="section section-md bg-gray-100">
        <div class="container-fluid">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Teams
                    </h5>
                </div>
            </article>
            <div class="product-cart">
                <div class="table-custom-responsive slider">
                    <table class="table-custom table-product">
                        <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>DURATION</th>
                            <th>PERCENT PER DAY</th>
                            <th>PROFIT PER HOUR</th>
                            <th>FINAL PROFIT</th>
                            <th>Time used</th>
                            <th>Collected balance</th>
                            <th>Pending balance</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($myPlans as $plan)
                            <tr id="plan{{$plan['id']}}">
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-name"><a class="product-cart-media"
                                                                      href="product-page.html">
                                            <img style="max-width: 80%"
                                                 src="{{url('/images/plan_logos/' .$plan['plan']['logo']['path'])}}" alt=""></a>
                                        <p class="product-cart-title"><a
                                                href="product-page.html">{{ $plan['name'] }}</a></p>
                                    </div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span>$ {{ $plan['cost'] }}</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span> {{ $plan['duration'] }} days</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span> {{ $plan['percent_per_day'] }} %</span></div>
                                </td>
                                <td>
                                    <div class="product-cart-price f-16"><span>$  {{ $plan['profit_per_hour'] }}</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span>$ {{ $plan['finally_profit'] }}</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span>{{ $plan['past_time'] }}</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price f-16"><span>$  {{ $plan['collected_profit'] }}</span></div>
                                </td>
                                <td class=" my-teams-listing-padding">
                                    <div class="product-cart-price  f-16"><span class="pending-balance">$  {{ $plan['pending_profit'] }}</span></div>
                                </td>

                                <td>
                                    <div>
                                        <form class="collectPendingProfit" method="post" action="{{route('collect')}}">
                                            @csrf
                                            <input type="hidden" name="user_plan_id" value="{{ $plan['id'] }}">
                                            <input class=" @if(!$plan['can_collect'])
                                                btn
                                                btn-secondary
                                                disable-btn
                                                disabled-button
                                                btn-lg
                                                mt-4
                                                @else
                                                button
                                                button-primary
                                                disable-btn
                                                btn-sm
                                                @endIf
                                               " type="submit" value="Collect"
                                                   @if(!$plan['can_collect']) disabled  @endif>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav class="pagination-wrap" aria-label="Page navigation">
                    <ul class="pagination">
                        {{ $myPlans->appends(request()->input())->links() }}
                    </ul>
                </nav>
            </div>
        </div>
    </section>
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="font_normal_text">--}}
{{--                    My Teams--}}
{{--                </div>--}}
{{--                @if(empty($myPlans))--}}
{{--                    @include('includes.noActiveSubscriptionDialog')--}}
{{--                @endif--}}
{{--                @foreach($myPlans as $plan)--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header font_small_text flex justify-content-between">--}}
{{--                            <div style="width: 150px">--}}
{{--                                {{ $plan['name'] }}--}}
{{--                            </div>--}}
{{--                            <div class="color_money" style="width: 60px">--}}
{{--                                {{ $plan['cost'] }}$--}}
{{--                            </div>--}}
{{--                            <div class="color_day" style="width: 100px">--}}
{{--                                {{ $plan['duration'] }} days--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="card-body flex justify-content-between">--}}
{{--                            <div>--}}
{{--                                <p class="color_day">Percent Per Day: {{ $plan['percent_per_day'] }}</p>--}}
{{--                                <p class="color_money">Profit Per Hour: {{ $plan['profit_per_hour'] }}$</p>--}}
{{--                                <p class="color_profit font_small_text">Finally Profit: {{ $plan['finally_profit'] }}--}}
{{--                                    $</p>--}}
{{--                            </div>--}}
{{--                            <div style="background-image: url({{ '/images/' .$plan['plan']['logo']['path'] }});--}}
{{--                                background-size:contain; height: 100px; width: 100px;"--}}
{{--                                 title="Smart Invest Greetings To You"></div>--}}
{{--                            <div class="flex justify-content-between">--}}
{{--                                <div>--}}
{{--                                    <div style="width: 100px" class="color_5">Subscribed At:</div>--}}
{{--                                    <div style="width: 100px" class="color_5">Ended At:</div>--}}
{{--                                    <div style="width: 100px" class="color_5">Use Time:</div>--}}
{{--                                    <div style="width: 110px" class="color_5">Pending funds:</div>--}}
{{--                                    <div style="width: 110px; margin-top: 5px" class="color_5">Collected funds:</div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div>{{ $plan['created_at'] }}</div>--}}
{{--                                    <div>{{ $plan['ended_at'] }}</div>--}}
{{--                                    <div>{{ $plan['past_time'] }}</div>--}}
{{--                                    <div style="display: flex">--}}
{{--                                        <div>--}}
{{--                                            {{ $plan['pending_profit'] }}$--}}
{{--                                        </div>--}}
{{--                                        <div style="margin-left: 10px">--}}
{{--                                            <form class="collectPendingProfit" method="post" action="/collect">--}}
{{--                                                {{ csrf_field() }}--}}
{{--                                                <input type="hidden" name="user_plan_id" value="{{ $plan['id'] }}">--}}
{{--                                                <input type="submit" value="Collect"--}}
{{--                                                       @if(!$plan['can_collect']) disabled @endif>--}}
{{--                                            </form>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div>{{ $plan['collected_profit'] }}</div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--            {{ $myPlans->links() }}--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
