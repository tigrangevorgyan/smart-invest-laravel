@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Partnership rules
                    </h5>
                </div>
            </article>

            @if(\Illuminate\Support\Facades\Auth::check())
                <div class="contact-list">
                    <dl>
                        <dt>Your referral link</dt>
                        <dd><span></span>
                            {{ generateRefLink(\Illuminate\Support\Facades\Auth::user()) }}
                        </dd>
                    </dl>
                </div>
            @endif
            <div class="contact-list">
                <dl>
                    <dt>Information about partnership</dt>
                    <dd>The referral program is the main way to make money without investment.
                        Invite your friends and acquaintances to the project through your referral link and get an income!
                    </dd>
                    <dd>From each registered user through your referral link, you will receive a reward
                        for certain actions that he will perform.</dd>
                    <dd style="color: black; font-weight: bold;">*We always work on increasing count of paid actions.</dd>
                    <br>
                    <dt>List of paid actions</dt>
                    @foreach($settings as $index => $setting)
                        <dd>{{ $index + 1 }}. {{ $setting->display_text }} <span style="color: red">{{ $setting->value }}%</span></dd>
                    @endforeach
                    <br>
                    <span style="color: red">All this bonuses will be added to "For withdraw" balance.</span>
                </dl>
            </div>
{{--            <div class="contact-list">--}}
{{--                <dl>--}}
{{--                    <dt>Administration security</dt>--}}
{{--                    <dd>1. qwe</dd>--}}
{{--                    <dd>2. asd</dd>--}}
{{--                </dl>--}}
{{--            </div>--}}
        </div>
    </section>
@endsection
