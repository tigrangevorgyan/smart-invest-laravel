@extends('layouts.app')

@section('content')
    @include('modal.actionCompletedModal')
    @include('modal.actionNotCompletedModal')
    <section class="section section-md bg-gray-100">
        <div class="container">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Payable sites
                    </h5>
                </div>
                <div class="text-transform-none pb-2">All websites located in this section are already checked. They pay!</div>
            </article>
        @if($sites != null)
                <div class="product-cart">
                    <div class="table-custom-responsive slider">
                        <table class="table-custom table-product">
                            <thead>
                            <tr>
                                <th style="width: 50px">N:</th>
                                <th>Website</th>
                                <th>Using cryptocurrency</th>
                                <th>Free bonus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sites as $index => $site)
                                <tr>
                                    <td>{{ $index + 1 .'.' }}</td>
                                    <td>
                                        <a href="{{ $site['link'] }}" target="_blank">{{ $site['site_name'] }}</a>
                                    </td>
                                    <td>
                                        <div class="product-cart-price text-center">
                                            <span>{{ $site['using_cryptocurrency'] ? 'yes' : 'no' }}</span></div>
                                    </td>
                                    <td>
                                        <div class="product-cart-price text-center">
                                            <span>{{ $site['bonus'] ? 'yes' : 'no' }}</span></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <nav class="pagination-wrap" aria-label="Page navigation">
                        <ul class="pagination">
                            {{ $sites->appends(request()->input())->links() }}
                        </ul>
                    </nav>
                </div>
            @else
                @include('includes.content_blocked', [
                    'blockedContent' => $blockedContentInfo
                ])
            @endif
        </div>
    </section>
@endsection
