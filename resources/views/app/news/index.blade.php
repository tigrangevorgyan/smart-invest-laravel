@extends('layouts.app')

@section('content')
    <section class="section section-md bg-gray-100 position-relative">
        <div class="container">
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">News
                    </h5>
                </div>
            </article>
            @foreach($news as $singleNews)
                <div class="row row-30">
                    <div class="col-md-6 col-lg-4">
                        <article class="product">
                            <header class="product-header">
                                <div class="badge badge-red">{{ $singleNews['created_at'] }}<span class="icon material-icons-whatshot"></span>
                                </div>
                                <div class="product-figure"><img src="{{url('/images/static_icons/' .$singleNews['attachment']['path']) }}" alt="" width="200px" height="200px"/></div>
                            </header>
                            <footer class="product-content">
                                <h6 class="product-title"><a href="/news/{{ $singleNews['id'] }}">{{ $singleNews['title'] }}</a></h6>
                                <p style="height: 100px">
                                    {{ $singleNews['description'] }}
                                </p>
                            </footer>
                        </article>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
