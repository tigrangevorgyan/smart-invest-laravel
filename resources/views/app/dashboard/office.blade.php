@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="font_normal_text">
                Transactions
            </div>
            <div style="width: 100%; margin-top: 20px" class="flex">
                <div style="width: 50%; margin-right: 20px;">
                    <div class="flex justify-content-between">
                        <div class="font_small_text">
                            Contracts(last 5)
                        </div>
                        <div class="font_small_text">
                            <a href="">Total {{ $plans['total'] }}</a>
                        </div>
                    </div>
                    @foreach($plans['items'] as $plan)
                        <div class="card col-md-12">
                            <div class="color_5">
                                {{ $plan['created_at'] }}
                            </div>
                            <div>
                                Signed contract with {{ $plan['name'] }}.
                            </div>
                            <div style="color: orange">
                                <i class="fa fa-shopping-basket"></i> -{{ $plan['cost'] }}$
                            </div>
                        </div>
                    @endforeach
                </div>
                <div style="width: 50%">
                    <div class="flex justify-content-between">
                        <div class="font_small_text">
                            Collects(last 5)
                        </div>
                        <div class="font_small_text">
                            <a href="">Total {{ $collects['total'] }}</a>
                        </div>
                    </div>
                    @foreach($collects['items'] as $collect)
                        <div class="card col-md-12">
                            <div class="color_5">
                                {{ $collect['created_at'] }}
                            </div>
                            <div>
                                Collected
                            </div>
                            <div style="color: green">
                                <i class="fa fa-money"></i> +{{ $collect['amount'] }}$
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div style="width: 100%; margin-top: 20px" class="flex">
                <div style="width: 50%; margin-right: 20px;">
                    <div class="flex justify-content-between">
                        <div class="font_small_text">
                            Payments(last 5)
                        </div>
                        <div class="font_small_text">
                            <a href="">Total {{ $payments['total'] }}</a>
                        </div>
                    </div>
                    @foreach($payments['items'] as $payment)
                        <div class="card col-md-12">
                            <div class="color_5">
                                {{ $payment['created_at'] }}
                            </div>
                            <div>
                                {{ $payment['system_number'] }}
                            </div>
                            <div style="color: orange">
                                <i class="fa fa-shopping-basket"></i> +{{ $payment['amount'] }}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div style="width: 50%">
                    <div class="flex justify-content-between">
                        <div class="font_small_text">
                            Withdraws(last 5)
                        </div>
                        <div class="font_small_text">
                            <a href="">Total {{ $withdraws['total'] }}</a>
                        </div>
                    </div>
                    @foreach($withdraws['items'] as $withdraw)
                        <div class="card col-md-12">
                            <div class="color_5">
                                {{ $withdraw['created_at'] }}
                            </div>
                            <div>
                                {{ $withdraw['system_number'] }}
                            </div>
                            <div style="color: green">
                                <i class="fa fa-money"></i> -{{ $withdraw['amount'] }}$
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

