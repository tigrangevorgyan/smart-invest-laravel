@extends('layouts.app')

@section('content')
    @include('modal.actionCompletedModal')
    @include('modal.actionNotCompletedModal')
    <section class="section section-md bg-gray-100 position-relative">
        <div class="container">
            <!-- Heading Component-->
            <article class="heading-component">
                <div class="heading-component-inner">
                    <h5 class="heading-component-title">Teams
                    </h5>
                </div>
            </article>
            <div class="product-cart">
                <div class="table-custom-responsive slider">
                    <table class="table-custom table-product">
                        <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>DURATION</th>
                            <th>PERCENT PER DAY</th>
                            <th>PROFIT PER HOUR</th>
                            <th>FINAL PROFIT</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($plans as $plan)
                        <tr>
                            <td>
                                <div class="product-cart-name"><a class="product-cart-media" href="product-page.html">
                                        <img style="max-width: 80%" src="{{url('/images/plan_logos/' .$plan['logo']['path']) }}" alt=""></a>
                                    <p class="product-cart-title"><a href="product-page.html">{{ $plan['name'] }}</a></p>
                                </div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$ {{ $plan['cost'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span> {{ $plan['duration'] }} days</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span> {{ $plan['percent_per_day'] }} %</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$  {{ $plan['profit_per_hour'] }}</span></div>
                            </td>
                            <td>
                                <div class="product-cart-price"><span>$ {{ $plan['finally_profit'] }}</span></div>
                            </td>
                            <td>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                <div class="product-cart-delete">
                                    <form class="subscribeToTeam" method="post" action="{{route('plan_user')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="plan_id" value="{{ $plan['id'] }}">
                                        <input class="button button-primary btn-sm" type="submit" value="Subscribe" title="{{ $plan['cost'] }}$">
                                    </form>
                                </div>
                                @else
                                    <div>
                                        <div>
                                            <a href="{{route('login-page')}}" style="font-size: 14px" class="button pr-2 pl-2  button-primary">Login/Register</a>
                                        </div>
                                    </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
