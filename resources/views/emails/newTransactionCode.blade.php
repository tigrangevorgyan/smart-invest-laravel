@extends('emails.commonEmailTemplate')

@section('content')
    Hi {{ $user->full_name }}
    Your transaction code
    <div>
        {{ $user->transaction_code }}
    </div>
@endsection
