<section class="section swiper-container swiper-slider swiper-classic bg-gray-2"
         data-loop="true" data-autoplay="4000" data-simulate-touch="false" data-slide-effect="fade">
    <div class="swiper-wrapper">
        <div class="swiper-slide text-center" data-slide-bg="images/album1-image-2-1199x800-original.jpg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="swiper-slide-caption">
                            <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Win with your favorite sport clubs!</h1>
                            <h4 data-caption-animate="fadeInUp" data-caption-delay="200">
                                JOIN US
                            </h4>
                            <a class="button button-primary mt-0" data-caption-animate="fadeInUp" data-caption-delay="300" href="{{route('aboutUs')}}">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="images/album6-image-1-1190x800-original.jpg">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-xl-5">
                        <div class="swiper-slide-caption">
                            <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Enjoy and earn!</h1>
                            <h4 data-caption-animate="fadeInUp" data-caption-delay="200">JOIN US</h4><a class="button button-primary" data-caption-animate="fadeInUp" data-caption-delay="300" href="{{route('aboutUs')}}">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="images/album6-image-2-1199x800-original.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="swiper-slide-caption">
                            <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Best Earning Website!</h1>
                            <h4 data-caption-animate="fadeInUp" data-caption-delay="200">join us<br class="d-none d-xl-block"></h4><a class="button button-primary" data-caption-animate="fadeInUp" data-caption-delay="300" href="{{route('aboutUs')}}">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-button swiper-button-prev"></div>
    <div class="swiper-button swiper-button-next"></div>
    <div class="swiper-pagination"></div>
</section>
