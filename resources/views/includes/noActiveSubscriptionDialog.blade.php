<div style="margin-top: 30px;">
    <div class="flex">
        <div class="font_normal_text color_5">
            Admin:
        </div>
        <div class="font_normal_text" style="color: red">
            &nbsp;
            You don`t have active subscription. Why you was registered?
        </div>
    </div>
    <div class="flex">
        <div class="font_normal_text color_4">
            You:
        </div>
        <div class="font_normal_text">
            &nbsp;
            For enjoy my time and earn money.
        </div>
    </div>
    <div class="flex">
        <div class="font_normal_text color_5">
            Admin:
        </div>
        <div class="font_normal_text" style="color: red">
            &nbsp;
            Great!!! Go to <a href="/plan">Teams</a> section and make your first subscription.
        </div>
    </div>
    <div class="flex">
        <div class="font_normal_text color_4">
            You:
        </div>
        <div class="font_normal_text">
            &nbsp;
            What if I will lose my money?
        </div>
    </div>
    <div class="flex">
        <div class="font_normal_text color_5">
            Admin:
        </div>
        <div class="font_normal_text" style="color: red">
            &nbsp;
            It can not happen.
        </div>
    </div>
    <div class="flex">
        <div class="font_normal_text color_4">
            You:
        </div>
        <div class="font_normal_text">
            &nbsp;
            Cool!!! Thank you.
        </div>
    </div>
</div>
