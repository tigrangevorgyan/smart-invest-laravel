<div class="aside-component">
    <!-- Heading Component-->
    <article class="heading-component">
        <div class="heading-component-inner">
            <h5 class="heading-component-title">Standings
            </h5><a class="button button-xs button-gray-outline" href="standings.html">Full Standings</a>
        </div>
    </article>
    <!-- Table team-->
    <div class="table-custom-responsive">
        <table class="table-custom table-standings table-classic">
            <thead>
            <tr>
                <th colspan="2">Team Position</th>
                <th title="Current">C</th>
                <th title="Finished">F</th>
                <th title="Point">Pts</th>
            </tr>
            </thead>
            <tbody>
            @foreach($plans as $index => $plan)
                <tr>
                    <td><span>{{ $index + 1 }}</span></td>
                    <td class="team-inline">
                        <div class="team-figure"><img src="images/plan_logos/{{ $plan['logo']['path'] }}" alt="" width="31"
                                                      height="41"/>
                        </div>
                        <div class="team-title">
                            <div class="team-name">{{ $plan['name'] }}</div>
                            <div class="team-country">{{ $plan['country'] }}</div>
                        </div>
                    </td>
                    <td>{{ $plan['current_users_count'] }}</td>
                    <td>{{ $plan['finished_users_count'] }}</td>
                    <td>{{ $plan['users_count'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
