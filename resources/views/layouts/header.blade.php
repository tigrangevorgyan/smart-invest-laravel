<header class="section  rd-navbar-dark">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
             data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed"
             data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
             data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static"
             data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="166px" data-xl-stick-up-offset="166px"
             data-xxl-stick-up-offset="166px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-main"><span></span></button>
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel-inner container nav-container">
                    <div class="rd-navbar-collapse rd-navbar-panel-item rd-navbar-panel-item-left">
                        <!-- Owl Carousel-->
                        <div class="owl-carousel-navbar owl-carousel-inline-outer">
                            <div class="owl-inline-nav">
                                <button class="owl-arrow owl-arrow-prev"></button>
                                <button class="owl-arrow owl-arrow-next"></button>
                            </div>
                            <div class="owl-carousel-inline-wrap">
                                <div class="owl-carousel owl-carousel-inline" data-items="1" data-dots="false"
                                     data-nav="true" data-autoplay="true" data-autoplay-speed="3200"
                                     data-stage-padding="0" data-loop="true" data-margin="10" data-mouse-drag="false"
                                     data-touch-drag="false" data-nav-custom=".owl-carousel-navbar">
                                    <!-- Post Inline-->
                                    <article class="post-inline">
                                        <time class="post-inline-time">Server time</time>
                                        <p class="post-inline-title">{{ \Carbon\Carbon::now()->toDateTimeString() }}</p>
                                    </article>
                                    <!-- Post Inline-->
                                    <article class="post-inline">
                                        <time class="post-inline-time">We opened at</time>
                                        <p class="post-inline-title">{{ \Carbon\Carbon::parse("2020-09-01")->toDateString() }}</p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rd-navbar-panel-item  rd-navbar-panel-item-right">
                        @if(!\Illuminate\Support\Facades\Auth::check())
                            <ul class="list-inline list-inline-bordered">
                                <li><a class="link link-icon link-icon-left link-classic"
                                       href="{{route('login-page')}}"><span class="icon fl-bigmug-line-login12"></span>
                                            <span class="link-icon-text">Your Account</span>
                                    </a></li>
                            </ul>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <ul class="list-inline account-properties-container list-inline-bordered">
                                <div class="rd-navbar-main-bottom mr-5 rd-navbar-darker">
                                    <div class="rd-navbar-main-container container">
                                        <!-- RD Navbar Nav-->
                                        <ul class="rd-navbar-nav">
                                            <li class="rd-nav-item" style="background-color: #2c2d31"><a class="rd-nav-link" href="#">
                                                    {{\Illuminate\Support\Facades\Auth::user()->first_name .' ' . \Illuminate\Support\Facades\Auth::user()->last_name}}</a>
                                                <ul class="rd-menu rd-navbar-dropdown">
                                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                                    href="{{route('payment-settings')}}">Payment settings</a>
                                                    </li>
                                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                                    href="{{route('profile')}}">Profile</a>
                                                    </li>
                                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                                    href="{{route('my_plans', ['withCount' => true])}}">My teams</a>
                                                    </li>
                                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                                                    href="{{route('all_transactions')}}">Transaction history</a>
                                                    </li>
                                                    <li class="rd-dropdown-item">
                                                        <form action="{{ route('logout') }}" class="text-center"
                                                              method="post">
                                                            @csrf
                                                            <button
                                                                class="button mt-2 mb-2 pt-2 pl-3 pr-3 pb-2 button-default"
                                                                type="submit" value="Logout">Logout
                                                            </button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </ul>
                        @endif
                    </div>
                    <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1"
                         data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
                </div>
            </div>
            <div class="rd-navbar-main">
                <div class="rd-navbar-main-top">
                    <div class="rd-navbar-main-container container">
                        <!-- RD Navbar Brand-->
                        <div class="rd-navbar-brand"><a class="brand"
                                                        href="/"><img
                                    class="brand-logo " src="/images/logo-soccer-default-95x126.png" alt="" width="95"
                                    height="126"/></a>
                        </div>
                        <!-- RD Navbar List-->
                        <ul class="rd-navbar-list">
                            <li class="rd-navbar-list-item"><a target="_blank" class="rd-navbar-list-link" href="https://payeer.com/?partner=12637780"><img
                                        src="/images/partners/payeer_2.png" alt="" width="75" height="42"/></a>
                            </li>
{{--                            <li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="index.html#"><img--}}
{{--                                        src="/images/partners-2-inverse-88x45.png" alt="" width="88" height="45"/></a>--}}
{{--                            </li>--}}
{{--                            <li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="index.html#"><img--}}
{{--                                        src="/images/partners-3-inverse-79x52.png" alt="" width="79" height="52"/></a>--}}
{{--                            </li>--}}
                        </ul>

                        <div class="rd-navbar-search justify-content-center">
                            @if(\Illuminate\Support\Facades\Auth::check())
                                <div class="shop-cart-container">
                                    <div style="margin-bottom: 10px">
                                        <a href="/replenishment-page" style="color: white">
                                            <i class="fa fa-shopping-cart">
                                                <span style="margin-left: 5px">
                                                    {{ __('For shopping') }}
                                                </span>
                                                <span id="shoppingBalance" style="margin-left: 10px">
                                                {{ \Illuminate\Support\Facades\Auth::user()->shopping_balance }}
                                            </span>
                                            </i>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="/withdraw-page" style="color: white">
                                            <i class="fa fa-money">
                                                <span style="margin-left: 5px">
                                                    {{ __('For withdraw') }}
                                                </span>
                                                <span id="withdrawBalance" style="margin-left: 10px">
                                                {{ \Illuminate\Support\Facades\Auth::user()->withdraw_balance }}
                                            </span>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="rd-navbar-main-bottom rd-navbar-darker">
                    <div class="rd-navbar-main-container container">
                        <!-- RD Navbar Nav-->
                        <ul class="rd-navbar-nav">
                            <li class="rd-nav-item active"><a class="rd-nav-link" href="{{route('home-page')}}">Home</a></li>
                            <li class="rd-nav-item"><a class="rd-nav-link" href="/news">News</a>

                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link" href="#">Earn</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{route('plan')}}">Teams</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{route('payableSites', ['withCount' => true])}}">
                                            Payable partners
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <li class="rd-nav-item"><a class="rd-nav-link" href="#">Transactions</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/collect">Collects</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/replenishment">Replenishments</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/withdraw">Withdraws</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{ route("replenishmentBonuses") }}">Replenishment Bonuses</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link" href="#">Partners</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="shop-elements.html">My referrals</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{ route('partnershipRules') }}">Partnership rules</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="list-shop.html">Referral statistic</a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        <div class="rd-navbar-main-element">
                            <ul class="list-inline list-inline-sm">
                                <li><a class="icon icon-xs icon-light fa fa-facebook" href="/"></a></li>
                                <li><a class="icon icon-xs icon-light fa fa-twitter" href="/"></a></li>
                                <li><a class="icon icon-xs icon-light fa fa-google-plus" href="/"></a></li>
                                <li><a class="icon icon-xs icon-light fa fa-instagram" href="/"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

