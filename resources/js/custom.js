$(".subscribeToTeam").submit(function(e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            if (data['success_purchased'] === true) {
                $("#actionCompletedModalText").text("GREAT!!! Your contract with " + data['plan_name'] + " has begun.")
                $('#actionCompletedModal').modal('show')
                $('#shoppingBalance').text(data['shopping_balance'])
            }
        },
        error: function (data) {
            $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"])
            $('#actionNotCompletedModal').modal('show')
        }
    });
});

$(".collectPendingProfit").submit(function (e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            $('#plan'+ data['id']).find('.pending-balance').html('$ 0');
            var disableButton = $('#plan'+ data['id']).find('.disable-btn');
            disableButton.removeClass("button button-primary disable-btn btn-sm");
            disableButton.addClass("disabled-button disable-btn mt-4 btn-secondary btn btn-lg");
            disableButton.attr('disabled', 'disabled');

            if (data['success_collected'] === true) {
                $("#actionCompletedModalText").text(data['amount'] + "$ was collected.");
                $('#actionCompletedModal').modal('show');
                $('#withdrawBalance').text(data['withdraw_balance'])
            }
        },
        error: function (data) {
            $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"])
            $('#actionNotCompletedModal').modal('show')
        }
    });
})

$("#register-form").submit(function(e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            if(data){
                window.location = '/'
            }
        },
        error: function (data) {
         returnErrorMessages(data.responseJSON.errors)
        }
    });
});

$("#payeerSubmit").submit(function(e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data) {
            $('#withdrawBalance').text(data['withdraw_balance'])
            $("#actionCompletedModalText").text("Payment done. Please check your Payeer balance.");
            $('#actionCompletedModal').modal('show');
        },
        error: function (data) {
            $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"])
            $('#actionNotCompletedModal').modal('show')
        }
    });
});

$("#unblockPayableSitesContent").submit(function(e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            $("#actionCompletedModalText").html("Information unblocked." + "</br>" + " 2 sec...");
            $('#actionCompletedModal').modal('show');
            setTimeout(function () {
                window.location.href = '/payable-partners?withCount=1'
            }, 2000)
        },
        error: function (data) {
            $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"])
            $('#actionNotCompletedModal').modal('show')
        }
    });
});

function returnErrorMessages(errors) {
    $.each(errors, function(index, value) {
        $('#'+ index).html('');
        $('#'+ index).html(value[0]);
    });
}

$("#payeerReplenishment").keypress(function (e) {
    if (!this.value.includes(".") && this.value.length > 0 && e.which == 46) return true;
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
});


$("#user-profile-update").submit(function(e) {
    e.preventDefault();

    let form = $(this);
    var data = {
        'first_name': form.find('#profile-first-name').val(),
        'last_name': form.find('#profile-last-name').val(),
    }
    if (form.find('#profile-password').val() || form.find('#profile-old-password').val()){
                data.password = form.find('#profile-password').val();
                data.oldPassword = form.find('#profile-old-password').val()
    }
    $.ajax({
        type: 'PUT',
        url: form.attr('action'),
        data: JSON.stringify(data),
        contentType: 'application/json',
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data)
        {
            if (data){
                document.location.href = '/profile'
            }
        },
        error: function (response) {
            if (response.responseJSON.errorMessage != undefined){
                form.find('#changePassword').text(response.responseJSON.errorMessage.changePassword)
            }
            if (response.responseJSON.errors){
                returnErrorMessages(response.responseJSON.errors)
            }
        }
    });
});

$('#send-new-transaction').submit(function(e) {
    e.preventDefault();
    let form =  $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data) {
            if (data['successfully_changed'] === true) {
                $("#actionCompletedModalText").text('An email has been sent the transactions code.');
                $('#actionCompletedModal').modal('show');
            }
        },
        error: function (data) {
            returnErrorMessages(data.responseJSON.errors)
        }
    });

});

$('#send-contact-us-email').submit(function(e) {
    e.preventDefault();
    let form =  $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data) {
            if (data['success'] === true) {
                $("#actionCompletedModalText").text('Our support team will response you during 4 hours.');
                $('#actionCompletedModal').modal('show');
            }
        },
        error: function (data) {
            returnErrorMessages(data.responseJSON.errors)
        }
    });

});

var animatedSlider = [];
let alreadySliderChecked = false;
$(window).scroll(function () {
    if ($(window).width() < 1200) {
        var hT2 = $('.slider').offset().top,
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wH + wS > hT2 && !alreadySliderChecked) {
            coachSliders($('.slider'))
        }
    }
    function coachSliders(element){
        var total = element.length;
        $.each(element , function (index){
            if (index !== total - 1){
                animateSlider($(this), index)
            }else {
                animateSlider($(this) , index)
            }
        })
        function animateSlider(val, index){
            var found = jQuery.inArray(index, animatedSlider);
            if (wH + wS > val.offset().top){
                if (found === -1) {
                    animatedSlider.push(index);
                    let elementWidth = val.css('width')
                    let width = parseInt(elementWidth.replace('px', ''))
                    let scroll = parseInt(val.offset().left) + (width + (width/2) + 50);
                    setTimeout(() => {
                        val.animate({scrollLeft: scroll }, 5000);
                    }, 2000)
                    setTimeout(() => {
                        val.animate({ scrollLeft: 0}, 2000)
                    }, 3000)
                    if (index === total -1){
                        alreadySliderChecked = true
                    }
                }
            }
            return false
        }
        return false;
    }
});
