<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@welcomePage');

Auth::routes();
Route::get('/cache-clear', function () {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear') ;
    \Illuminate\Support\Facades\Artisan::call('config:cache') ;
});
Route::get('/login-page', function (){
    return view('auth.login_register');
})->name('login-page');

Route::get('/home-page', function (){
    return redirect()->to('/');
})->name('home-page');

Route::group(['namespace' => 'Api\V1'], function () {
    Route::group(['namespace' => 'CommonPage'], function () {
        Route::get("about-us", "CommonPageController@aboutUs")->name("aboutUs");
        Route::get("faq", "CommonPageController@faq")->name("faq");
        Route::get("contact-us", "CommonPageController@contactUs")->name("contactUs");
        Route::post("contact-us", "CommonPageController@storeContactUs")->name("contactAsStore");
        Route::get("privacy-policy", "CommonPageController@privacyPolicy")->name("privacyPolicy");
    });
});

Route::group(['namespace' => 'Api\V1'], function () {
    Route::group(['namespace' => 'Plan'], function () {
        Route::get('plan', 'PlanController@index')->name('plan');
    });
    Route::group(['namespace' => 'PayableSite'], function () {
        Route::get('payable-partners', 'PayableSiteController@index')->name('payableSites');
    });
    Route::group(['namespace' => 'Referral'], function () {
        Route::get('rules', 'ReferralController@rules')->name('partnershipRules');
    });
    Route::group(['namespace' => 'News'], function () {
        Route::resource('news', 'NewsController')->only('index');
    });

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('auth/google', 'SocialController@redirectToGoogle')->name('google-login');
        Route::get('auth/google/callback', 'SocialController@handleGoogleCallback');

        Route::get('auth/facebook', 'SocialController@redirectToFacebook')->name('facebook-login');
        Route::get('login/facebook/callback','SocialController@handleFacebookCallback');
    });
});

Route::group(['namespace' => 'Api\V1', 'middleware' => 'auth'], function () {
    Route::group(['namespace' => 'Dashboard'], function () {
        Route::get('office', 'DashboardController@office');
    });
    Route::group(['namespace' => 'Plan'], function () {
        Route::resource('plan', 'PlanController')->except('index');
    });
    Route::group(['namespace' => 'UserPlan'], function () {
        Route::get('my-teams', 'UserPlanController@myPlans')->name('my_plans');
        Route::post('planUser', 'UserPlanController@store')->name('plan_user');
        Route::post('planUser/collect', 'UserPlanController@collect');
    });

    Route::group(['namespace' => 'Transaction'], function () {
        Route::get('all-transactions', 'TransactionController@index')->name('all_transactions');
        Route::get('replenishment-page', 'TransactionController@replenishmentPage');
        Route::get('withdraw-page', 'TransactionController@withdrawPage')->name('withdrawPage');

        Route::group(['namespace' => 'Collect'], function () {
            Route::get('collect', 'CollectController@index')->name('collectIndex');
            Route::post('collect', 'CollectController@store')->name('collect');
        });
        Route::group(['namespace' => 'Replenishment'], function () {
            Route::get('replenishment', 'ReplenishmentController@index');
            Route::post('replenishment', 'ReplenishmentController@store');
        });
        Route::group(['namespace' => 'Withdraw'], function () {
            Route::get('withdraw', 'WithdrawController@index');
            Route::post('withdraw', 'WithdrawController@store');
        });
        Route::group(['namespace' => 'Bonus'], function () {
            Route::get('bonus/replenishment', 'BonusController@replenishmentBonus')->name("replenishmentBonuses");
        });
    });
    Route::group(['namespace' => 'User'], function () {
        Route::get('profile', 'UserController@profile')->name('profile');
        Route::get('payment-settings', 'UserController@paymentSettings')->name('payment-settings');
        Route::put('user/{user}', 'UserController@update');
        Route::put('user/{user}/payment-system-info', 'UserController@updatePaymentSystemInfo');
        Route::post('user/{user}/new-transaction-code', 'UserController@changeTransactionCode');
    });
    Route::group(['namespace' => 'PaidContent'], function () {
        Route::post('paidContent', 'PaidContentController@store');
    });
});

