<?php

use App\Models\Attachment;
use Illuminate\Database\Seeder;

class AttachmentSeeder extends Seeder
{
    public function run()
    {
        Attachment::create([
            'name'  => 'Real Madrid Logo',
            'path'  => 'real_logo.png'
        ]);

        Attachment::create([
            'name'  => 'Juventus Logo',
            'path'  => 'juventus_logo.png'
        ]);

        Attachment::create([
            'name'  => 'Liverpool Logo',
            'path'  => 'liverpool_logo.png'
        ]);

        Attachment::create([
            'name'  => 'News Logo',
            'path'  => 'news.jpg'
        ]);
    }
}
