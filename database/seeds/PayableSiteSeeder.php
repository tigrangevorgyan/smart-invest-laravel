<?php

use App\Models\PayableSite;
use Illuminate\Database\Seeder;

class PayableSiteSeeder extends Seeder
{
    public function run()
    {
        $sites = [
            [
                'site_name'             => 'MoreMoney',
                'link'                  => 'https://moremoney.io/?ref=258248',
                'using_cryptocurrency'  => true,
                'bonus'                 => true
            ],
            [
                'site_name'             => 'GraB.TC',
                'link'                  => 'https://grab.tc/?ref=24135',
                'using_cryptocurrency'  => true,
                'bonus'                 => true
            ],
            [
                'site_name'             => 'BitsRoll',
                'link'                  => 'https://bitsroll.com/?ref=5238',
                'using_cryptocurrency'  => true,
                'bonus'                 => true
            ],
            [
                'site_name'             => 'adBTC',
                'link'                  => 'https://r.adbtc.top/1755539',
                'using_cryptocurrency'  => true,
                'bonus'                 => true
            ],
            [
                'site_name'             => 'coinpayu',
                'link'                  => 'https://www.coinpayu.com/?r=tigran95',
                'using_cryptocurrency'  => true,
                'bonus'                 => false
            ]
        ];

        foreach ($sites as $site) {
            PayableSite::create($site);
        }
    }
}
