<?php

use App\Models\Attachment;
use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    public function run()
    {
        Plan::create([
            'name'              => 'Real Madrid',
            'country'           => 'Spain',
            'duration'          => $duration = 120,
            'cost'              => $cost = 120,
            'percent_per_day'   => $percentPerDay = 1,
            'profit_per_hour'   => getProfitPerHour($cost, $percentPerDay),
            'finally_profit'    => getFinallyProfit($duration, $cost, $percentPerDay),
            'logo_id'           => Attachment::findBy('path', 'real_logo.png')->id
        ]);

        Plan::create([
            'name'              => 'Juventus',
            'country'           => 'Italy',
            'duration'          => $duration = 120,
            'cost'              => $cost = 150,
            'percent_per_day'   => $percentPerDay = 1,
            'profit_per_hour'   => getProfitPerHour($cost, $percentPerDay),
            'finally_profit'    => getFinallyProfit($duration, $cost, $percentPerDay),
            'logo_id'           => Attachment::findBy('path', 'juventus_logo.png')->id
        ]);

        Plan::create([
            'name'              => 'Liverpool',
            'country'           => 'England',
            'duration'          => $duration = 120,
            'cost'              => $cost = 180,
            'percent_per_day'   => $percentPerDay = 1,
            'profit_per_hour'   => getProfitPerHour($cost, $percentPerDay),
            'finally_profit'    => getFinallyProfit($duration, $cost, $percentPerDay),
            'logo_id'           => Attachment::findBy('path', 'liverpool_logo.png')->id
        ]);
    }
}
