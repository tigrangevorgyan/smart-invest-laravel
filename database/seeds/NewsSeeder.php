<?php

use App\Models\Attachment;
use App\Models\News;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    public function run()
    {
        $news = [
            [
                'title'         => 'We started',
                'description'   => 'We already started our project. This is an investing game. Here you can have regularly profit. ' .
                    'Now we have 2 earning options - 1. subscribe teams, 2. unblock "Payable Partners" section. ' .
                    'More details about earning options you can read in "About Us" section.',
                'attachment_id' => Attachment::findBy('path', 'news.jpg')->id
            ],
            [
                'title'         => 'Payable partners',
                'description'   => 'Happy to notice you that our team already has benn added "Payable Partners" section. ' .
                    'Unblocking this section you can find sites list, which also pay people real money.',
                'attachment_id' => Attachment::findBy('path', 'news.jpg')->id
            ]
        ];

        foreach ($news as $singleData) {
            News::create([
                'title'         => $singleData['title'],
                'description'   => $singleData['description'],
                'attachment_id' => $singleData['attachment_id']
            ]);
        }
    }
}
