<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::unsetEventDispatcher();

        factory(User::class)->create([
            'first_name'    => 'Tigran',
            'last_name'     => 'Gevorgyan',
            'email'         => 'tigrangevorgyan95@gmail.com',
            'type'          => User::TYPES['user']
        ]);

        factory(User::class)->create([
            'first_name'    => 'Jivan',
            'last_name'     => 'Mikayelyan',
            'email'         => 'jivanmikayelyan15@gmail.com',
            'type'          => User::TYPES['user']
        ]);

        factory(User::class, 20)->create();
    }
}
