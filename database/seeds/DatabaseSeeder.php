<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(AttachmentSeeder::class);
        $this->call(AppSettingSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(PaidContentSeeder::class);
        $this->call(PayableSiteSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(UserPlanSeeder::class);
        $this->call(ReplenishmentSeeder::class);
        $this->call(WithdrawSeeder::class);
    }
}
