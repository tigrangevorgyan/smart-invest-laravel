<?php

use App\Models\AppSetting;
use Illuminate\Database\Seeder;

class AppSettingSeeder extends Seeder
{
    public function run()
    {
        foreach (AppSetting::SETTINGS as $setting => $index) {
            AppSetting::firstOrCreate([
                'key'   => $index,
                'value' => AppSetting::VALUES[$setting]
            ]);
        }
    }
}
