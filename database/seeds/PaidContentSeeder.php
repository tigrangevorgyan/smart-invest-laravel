<?php

use App\Models\PaidContent;
use Illuminate\Database\Seeder;

class PaidContentSeeder extends Seeder
{
    public function run()
    {
        $seedData = [
            [
                'type'      => PaidContent::TYPES['payable_sites'],
                'price'     => 1,
                'duration'  => 30
            ]
        ];

        foreach ($seedData as $singleData) {
            PaidContent::create([
                'type'      => $singleData['type'],
                'price'     => $singleData['price'],
                'duration'  => $singleData['duration']
            ]);
        }
    }
}
