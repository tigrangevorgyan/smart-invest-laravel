<?php

use App\Models\Replenishment;
use App\Models\User;
use Illuminate\Database\Seeder;

class ReplenishmentSeeder extends Seeder
{
    public function run()
    {
        foreach (User::all() as $user) {
            foreach (range(1, 10) as $index) {
                $user->replenishments()->create([
                    'amount'        => $amount = rand(100, 1000),
                    'commission'    => $amount * Replenishment::PAYEER_COMMISSION / 100,
                    'system_type'   => Replenishment::TYPES['payeer'],
                    'system_number' => "P123456789"
                ]);
            }
        }
    }
}
