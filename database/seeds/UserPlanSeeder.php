<?php

use App\Models\Plan;
use App\Models\User;
use App\Models\UserPlan;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserPlanSeeder extends Seeder
{
    public function run()
    {
        foreach (User::all() as $user) {
            foreach (range(1, 10) as $index) {
                $plan = Plan::inRandomOrder()->first();
                UserPlan::create([
                    'user_id'           => $user->id,
                    'plan_id'           => $plan->id,
                    'name'              => $plan->name,
                    'country'           => $plan->country,
                    'duration'          => $plan->duration,
                    'cost'              => $plan->cost,
                    'percent_per_day'   => $plan->percent_per_day,
                    'profit_per_hour'   => $plan->profit_per_hour,
                    'finally_profit'    => $plan->finally_profit,
                    'collected_profit'  => 0,
                    'ended_at'          => rand(0, 1) ? Carbon::now()->addDays($plan->duration) : Carbon::now()->subDays($plan->duration)
                ]);
            }
        }
    }
}
