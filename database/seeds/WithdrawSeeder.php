<?php

use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Database\Seeder;

class WithdrawSeeder extends Seeder
{
    public function run()
    {
        foreach (User::all() as $user) {
            foreach (range(1, 10) as $index) {
                $user->withdraws()->create([
                    'amount'        => $amount = rand(1, 100),
                    'commission'    => $amount * Withdraw::PAYEER_COMMISSION / 100,
                    'system_type'   => Withdraw::TYPES['payeer'],
                    'system_number' => "P123456789",
                    'status'        => array_rand(array_values(Withdraw::STATUSES))
                ]);
            }
        }
    }
}
