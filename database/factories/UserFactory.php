<?php

use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name'        => $name = $faker->firstName,
        'last_name'         => $faker->lastName,
        'email'             => $faker->unique()->safeEmail,
        'withdraw_balance'  => 10000,
        'shopping_balance'  => 20000,
        'password'          => bcrypt('asdasd'),
        'remember_token'    => Str::random(10),
        'transaction_code'  => generateTransactionCode(),
        'ref_nickname'      => strtolower($name) .'_' .Carbon::now()->timestamp
    ];
});
