<?php

use App\Models\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name'              => $faker->word,
        'cost'              => $cost = rand(1, 1000),
        'duration'          => $duration = rand(5, 365),
        'percent_for_day'   => $percentForDay = rand(10, 20) / 10,
        'profit_for_hour'   => round($cost * $percentForDay / 100 / 24, 2)
    ];
});
