<?php

use App\Models\PaymentSystemInfo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSystemInfosTable extends Migration
{
    public function up()
    {
        Schema::create('payment_system_infos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->smallInteger('type')->comment(json_encode(PaymentSystemInfo::TYPES));
            $table->string('account');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_system_infos');
    }
}
