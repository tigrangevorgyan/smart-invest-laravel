<?php

use App\Models\Withdraw;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->float('amount');
            $table->float('commission');

            $table->string('system_number')->nullable();
            $table->smallInteger('system_type')->comment(json_encode(Withdraw::TYPES));

            $table->smallInteger('status')->comment(json_encode(Withdraw::STATUSES));

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
