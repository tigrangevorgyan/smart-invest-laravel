<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayableSitesTable extends Migration
{
    public function up()
    {
        Schema::create('payable_sites', function (Blueprint $table) {
            $table->increments('id');

            $table->string('site_name');

            $table->string('icon')->nullable();
            $table->string('link');

            $table->boolean('using_cryptocurrency');
            $table->boolean('bonus');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payable_sites');
    }
}
