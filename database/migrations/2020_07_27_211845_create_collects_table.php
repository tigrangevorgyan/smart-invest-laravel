<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectsTable extends Migration
{
    public function up()
    {
        Schema::create('collects', function (Blueprint $table) {
            $table->increments('id');

            $table->float('amount');

            $table->unsignedInteger('user_plan_id')->nullable();
            $table->foreign('user_plan_id')->references('id')->on('user_plans')->onDelete('set null');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('collects');
    }
}
