<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPaidContentsTable extends Migration
{
    public function up()
    {
        Schema::create('user_paid_contents', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('paid_content_id')->nullable();
            $table->foreign('paid_content_id')->references('id')->on('paid_contents')->onDelete('cascade');

            $table->float('price')->nullable();
            $table->timestamp('expired_at')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_paid_contents');
    }
}
