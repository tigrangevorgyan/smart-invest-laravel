<?php

use App\Models\PaidContent;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaidContentsTable extends Migration
{
    public function up()
    {
        Schema::create('paid_contents', function (Blueprint $table) {
            $table->increments('id');

            $table->smallInteger('type')->comment(json_encode(PaidContent::TYPES));
            $table->float('price');
            $table->integer('duration')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('paid_contents');
    }
}
