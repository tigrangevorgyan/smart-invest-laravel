<?php

use App\Models\ReplenishmentBonus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReplenishmentBonusesTable extends Migration
{
    public function up()
    {
        Schema::create('replenishment_bonuses', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('replenishment_id')->nullable();
            $table->foreign('replenishment_id')->references('id')->on('replenishments')->onDelete('set null');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->smallInteger('type')->comment(json_encode(ReplenishmentBonus::TYPES));

            $table->float('amount');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('replenishment_bonuses');
    }
}
