<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name');
            $table->string('last_name');

            $table->unsignedInteger('inviter_id')->nullable();
            $table->foreign('inviter_id')->references('id')->on('users')->onDelete('set null');

            $table->string('ref_nickname')->unique()->nullable();
            $table->string('google_id')->unique()->nullable();
            $table->string('facebook_id')->unique()->nullable();
            $table->smallInteger('sign_up_method')->default(User::SIGN_UP_METHODS['registered'])->comment(json_encode(User::SIGN_UP_METHODS));
            $table->string('transaction_code');

            $table->smallInteger('type')->comment(json_encode(User::TYPES));

            $table->string('email')->unique();

            $table->double('shopping_balance')->default(0);
            $table->double('withdraw_balance')->default(0);

            $table->string('password');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
