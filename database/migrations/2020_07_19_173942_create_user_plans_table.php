<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->unsignedInteger('plan_id')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('set null');

            $table->string('name');
            $table->string('country');

            $table->integer('duration');

            $table->float('cost');
            $table->float('percent_per_day');
            $table->float('profit_per_hour');
            $table->float('finally_profit');

            $table->float('collected_profit');

            $table->timestamp('last_collected_at')->nullable();
            $table->timestamp('ended_at')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
