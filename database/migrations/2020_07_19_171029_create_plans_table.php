<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('country');

            $table->integer('duration');

            $table->float('cost');
            $table->float('percent_per_day');
            $table->float('profit_per_hour');
            $table->float('finally_profit');

            $table->unsignedInteger('logo_id')->nullable();
            $table->foreign('logo_id')->references('id')->on('attachments')->onDelete('set null');

            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    public function down()
    {
        Schema::enableForeignKeyConstraints();
        Schema::dropIfExists('plans');
        Schema::disableForeignKeyConstraints();
    }
}
