/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom.js":
/*!********************************!*\
  !*** ./resources/js/custom.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(".subscribeToTeam").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      if (data['success_purchased'] === true) {
        $("#actionCompletedModalText").text("GREAT!!! Your contract with " + data['plan_name'] + " has begun.");
        $('#actionCompletedModal').modal('show');
        $('#shoppingBalance').text(data['shopping_balance']);
      }
    },
    error: function error(data) {
      $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"]);
      $('#actionNotCompletedModal').modal('show');
    }
  });
});
$(".collectPendingProfit").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      $('#plan' + data['id']).find('.pending-balance').html('$ 0');
      var disableButton = $('#plan' + data['id']).find('.disable-btn');
      disableButton.removeClass("button button-primary disable-btn btn-sm");
      disableButton.addClass("disabled-button disable-btn mt-4 btn-secondary btn btn-lg");
      disableButton.attr('disabled', 'disabled');

      if (data['success_collected'] === true) {
        $("#actionCompletedModalText").text(data['amount'] + "$ was collected.");
        $('#actionCompletedModal').modal('show');
        $('#withdrawBalance').text(data['withdraw_balance']);
      }
    },
    error: function error(data) {
      $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"]);
      $('#actionNotCompletedModal').modal('show');
    }
  });
});
$("#register-form").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      if (data) {
        window.location = '/';
      }
    },
    error: function error(data) {
      returnErrorMessages(data.responseJSON.errors);
    }
  });
});
$("#payeerSubmit").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      $('#withdrawBalance').text(data['withdraw_balance']);
      $("#actionCompletedModalText").text("Payment done. Please check your Payeer balance.");
      $('#actionCompletedModal').modal('show');
    },
    error: function error(data) {
      $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"]);
      $('#actionNotCompletedModal').modal('show');
    }
  });
});
$("#unblockPayableSitesContent").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      $("#actionCompletedModalText").html("Information unblocked." + "</br>" + " 2 sec...");
      $('#actionCompletedModal').modal('show');
      setTimeout(function () {
        window.location.href = '/payable-partners?withCount=1';
      }, 2000);
    },
    error: function error(data) {
      $("#actionNotCompletedModalText").text(data["responseJSON"]["errorMessage"]["store"]);
      $('#actionNotCompletedModal').modal('show');
    }
  });
});

function returnErrorMessages(errors) {
  $.each(errors, function (index, value) {
    $('#' + index).html('');
    $('#' + index).html(value[0]);
  });
}

$("#payeerReplenishment").keypress(function (e) {
  if (!this.value.includes(".") && this.value.length > 0 && e.which == 46) return true;
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
});
$("#user-profile-update").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  var data = {
    'first_name': form.find('#profile-first-name').val(),
    'last_name': form.find('#profile-last-name').val()
  };

  if (form.find('#profile-password').val() || form.find('#profile-old-password').val()) {
    data.password = form.find('#profile-password').val();
    data.oldPassword = form.find('#profile-old-password').val();
  }

  $.ajax({
    type: 'PUT',
    url: form.attr('action'),
    data: JSON.stringify(data),
    contentType: 'application/json',
    processData: false,
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function success(data) {
      if (data) {
        document.location.href = '/profile';
      }
    },
    error: function error(response) {
      if (response.responseJSON.errorMessage != undefined) {
        form.find('#changePassword').text(response.responseJSON.errorMessage.changePassword);
      }

      if (response.responseJSON.errors) {
        returnErrorMessages(response.responseJSON.errors);
      }
    }
  });
});
$('#send-new-transaction').submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      if (data['successfully_changed'] === true) {
        $("#actionCompletedModalText").text('An email has been sent the transactions code.');
        $('#actionCompletedModal').modal('show');
      }
    },
    error: function error(data) {
      returnErrorMessages(data.responseJSON.errors);
    }
  });
});
$('#send-contact-us-email').submit(function (e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function success(data) {
      if (data['success'] === true) {
        $("#actionCompletedModalText").text('Our support team will response you during 4 hours.');
        $('#actionCompletedModal').modal('show');
      }
    },
    error: function error(data) {
      returnErrorMessages(data.responseJSON.errors);
    }
  });
});
var animatedSlider = [];
var alreadySliderChecked = false;
$(window).scroll(function () {
  if ($(window).width() < 1200) {
    var hT2 = $('.slider').offset().top,
        wH = $(window).height(),
        wS = $(this).scrollTop();

    if (wH + wS > hT2 && !alreadySliderChecked) {
      coachSliders($('.slider'));
    }
  }

  function coachSliders(element) {
    var total = element.length;
    $.each(element, function (index) {
      if (index !== total - 1) {
        animateSlider($(this), index);
      } else {
        animateSlider($(this), index);
      }
    });

    function animateSlider(val, index) {
      var found = jQuery.inArray(index, animatedSlider);

      if (wH + wS > val.offset().top) {
        if (found === -1) {
          animatedSlider.push(index);
          var elementWidth = val.css('width');
          var width = parseInt(elementWidth.replace('px', ''));
          var scroll = parseInt(val.offset().left) + (width + width / 2 + 50);
          setTimeout(function () {
            val.animate({
              scrollLeft: scroll
            }, 5000);
          }, 2000);
          setTimeout(function () {
            val.animate({
              scrollLeft: 0
            }, 2000);
          }, 3000);

          if (index === total - 1) {
            alreadySliderChecked = true;
          }
        }
      }

      return false;
    }

    return false;
  }
});

/***/ }),

/***/ 1:
/*!**************************************!*\
  !*** multi ./resources/js/custom.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/smart-invest-laravel/resources/js/custom.js */"./resources/js/custom.js");


/***/ })

/******/ });