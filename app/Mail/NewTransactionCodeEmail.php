<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewTransactionCodeEmail extends Mailable
{
    use Queueable, SerializesModels;

    private const SUBJECT = 'New transaction code';
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->to($this->user->email)
            ->subject(self::SUBJECT)
            ->view('emails.newTransactionCode', [
                'user' => $this->user
            ]);
    }
}
