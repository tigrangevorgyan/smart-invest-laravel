<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $subjectTitle;
    private $text;
    private $email;

    public function __construct(string $subject , string $text, string $email)
    {
        $this->subjectTitle = $subject;
        $this->text = $text;
        $this->email = $email;
    }

    public function build()
    {
        return $this->to(config('app.support_email'))
            ->subject($this->subjectTitle)
            ->replyTo($this->email)
            ->view('emails.contactUs', [
                'text' => $this->text
            ]);
    }
}
