<?php

namespace App\Listeners\UserCreated;

use App\Events\UserCreated;
use App\Models\Referral;

class AttachReferrals
{
    public function handle(UserCreated $event)
    {
        if ($event->user->inviter) {
            Referral::create([
                'user_id'   => $event->user->inviter_id,
                'child_id'  => $event->user->id,
                'level'     => Referral::LEVELS['first']
            ]);
            if ($event->user->inviter->inviter) {
                Referral::create([
                    'user_id'   => $event->user->inviter->inviter_id,
                    'child_id'  => $event->user->id,
                    'level'     => Referral::LEVELS['second']
                ]);
            }
        }
    }
}
