<?php

namespace App\Listeners\UserPlanCreated;

use App\Events\UserPlanCreated;

class UpdateUserBalance
{
    public function handle(UserPlanCreated $event)
    {
        $event->userPlan->user->decrement('shopping_balance', $event->userPlan->cost);
    }
}
