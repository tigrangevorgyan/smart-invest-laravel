<?php

namespace App\Listeners\WithdrawCreated;

use App\Events\WithdrawCreated;

class DecrementUserWithdrawBalance
{
    public function handle(WithdrawCreated $event)
    {
        $event->withdraw->user->decrement('withdraw_balance', $event->withdraw->amount + $event->withdraw->commission);
    }
}
