<?php

namespace App\Listeners\ReplenishmentCreated;

use App\Events\ReplenishmentCreated;

class AddUserShoppingBalance
{
    public function handle(ReplenishmentCreated $event)
    {
        $event->replenishment->user->increment('shopping_balance', $event->replenishment->amount);
    }
}
