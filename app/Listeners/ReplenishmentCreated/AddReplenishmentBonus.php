<?php

namespace App\Listeners\ReplenishmentCreated;

use App\Events\ReplenishmentCreated;
use App\Models\AppSetting;
use App\Models\Referral;
use App\Models\ReplenishmentBonus;

class AddReplenishmentBonus
{
    public function handle(ReplenishmentCreated $event)
    {
        $author = $event->replenishment->user;

        foreach ($author->parents as $parent) {
            if ($parent->pivot->level == Referral::LEVELS['first']) {
                $bonusAmount = getPercent(
                    $event->replenishment->amount + $event->replenishment->commission,
                    AppSetting::findBy('key', AppSetting::SETTINGS['replenishment_bonus_from_1_level_ref'])->value
                );
                $type = ReplenishmentBonus::TYPES['first_level'];
            } else {
                $bonusAmount = getPercent(
                    $event->replenishment->amount + $event->replenishment->commission,
                    AppSetting::findBy('key', AppSetting::SETTINGS['replenishment_bonus_from_2_level_ref'])->value
                );
                $type = ReplenishmentBonus::TYPES['second_level'];
            }

            ReplenishmentBonus::create([
                'replenishment_id'  => $event->replenishment->id,
                'user_id'           => $parent->id,
                'type'              => $type,
                'amount'            => $bonusAmount
            ]);
        }
    }
}
