<?php

namespace App\Listeners\UserCreating;

use App\Events\UserCreating;

class SetTransactionCode
{
    public function handle(UserCreating $event)
    {
        $event->user->transaction_code = generateTransactionCode();
    }
}
