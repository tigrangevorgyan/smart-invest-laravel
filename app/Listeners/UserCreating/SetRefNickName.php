<?php

namespace App\Listeners\UserCreating;

use App\Events\UserCreating;
use Carbon\Carbon;

class SetRefNickName
{
    public function handle(UserCreating $event)
    {
        $event->user->ref_nickname = strtolower($event->user->first_name) .'_' .Carbon::now()->timestamp;
    }
}
