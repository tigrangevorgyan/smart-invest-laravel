<?php

namespace App\Listeners\UserPaidContentCreated;

use App\Events\UserPaidContentCreated;

class UpdateUserBalance
{
    public function handle(UserPaidContentCreated $event)
    {
        $event->userPaidContent->user->decrement('shopping_balance', $event->userPaidContent->price);
    }
}
