<?php

namespace App\Listeners\CollectCreated;

use App\Events\CollectCreated;
use Carbon\Carbon;

class UpdateUserPlan
{
    public function handle(CollectCreated $event)
    {
        $event->collect->userPlan->update([
            'collected_profit'  => $event->collect->userPlan->collected_profit += $event->collect->amount,
            'last_collected_at' => Carbon::now()
        ]);
    }
}
