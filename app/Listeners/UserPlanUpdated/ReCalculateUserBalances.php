<?php

namespace App\Listeners\UserPlanUpdated;

use App\Events\UserPlanUpdated;

class ReCalculateUserBalances
{
    public function handle(UserPlanUpdated $event)
    {
        if ($event->userPlan->isDirty('collected_profit')) {
            $newCollectedProfit = $event->userPlan->collected_profit;
            $oldCollectedProfit = $event->userPlan->getOriginal('collected_profit');
            $diff = round($newCollectedProfit - $oldCollectedProfit, 2);

            $event->userPlan->user->increment('withdraw_balance', $diff);
        }
    }
}
