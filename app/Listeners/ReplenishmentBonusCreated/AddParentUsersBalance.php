<?php

namespace App\Listeners\ReplenishmentBonusCreated;

use App\Events\ReplenishmentBonusCreated;

class AddParentUsersBalance
{
    public function handle(ReplenishmentBonusCreated $event)
    {
        $event->replenishmentBonus->user->increment('withdraw_balance', $event->replenishmentBonus->amount);
    }
}
