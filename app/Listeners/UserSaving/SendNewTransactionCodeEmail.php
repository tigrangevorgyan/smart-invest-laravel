<?php

namespace App\Listeners\UserSaving;

use App\Events\UserSaving;
use App\Mail\NewTransactionCodeEmail;
use Illuminate\Support\Facades\Mail;

class SendNewTransactionCodeEmail
{
    public function handle(UserSaving $event)
    {
        if ($event->user->isDirty('transaction_code')) {
            Mail::send(new NewTransactionCodeEmail($event->user));
        }
    }
}
