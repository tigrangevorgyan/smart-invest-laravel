<?php

namespace App\Listeners\UserSaving;

use App\Events\UserSaving;

class BcryptPassword
{
    public function handle(UserSaving $event)
    {
        if ((!$event->user->id || $event->user->isDirty('password')) && request()->password){
            $event->user->password = bcrypt(request()->password);
        }
    }
}
