<?php

namespace App\Models;

use App\Events\ReplenishmentCreated;
use App\Models\Fragments\Replenishment\Getters;
use App\Models\Fragments\Replenishment\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property User user
 * @property float amount
 * @property int system_type
 * @property int id
 * @property float commission
 */
class Replenishment extends BaseModel
{
    use Relations, Getters;

    public const PAYEER_COMMISSION = 1;
    public const MIN = 0.1;
    public const MAX = 10000;

    protected $fillable = [
        'user_id',
        'amount',
        'system_type',
        'system_number',
        'commission'
    ];

    public const TYPES = [
        'payeer' => 1
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('availableForAuthUser', function (Builder $query) {
            if (!Auth::user()) return $query;

            if (Auth::user()->isStandardUser()) return $query->where('user_id', Auth::id());

            return $query;
        });
    }

    protected $dispatchesEvents = [
        'created' => ReplenishmentCreated::class
    ];
}
