<?php

namespace App\Models\Fragments\UserPlan;

trait Details
{
    public function canCollect(): bool
    {
        return $this->getPendingProfit() > 0;
    }
}
