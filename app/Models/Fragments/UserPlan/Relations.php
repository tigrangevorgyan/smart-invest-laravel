<?php

namespace App\Models\Fragments\UserPlan;

use App\Models\Collect;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait Relations
{
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function plan(): BelongsTo
    {
        return $this->belongsTo(Plan::class);
    }

    public function collects(): HasMany
    {
        return $this->hasMany(Collect::class);
    }
}
