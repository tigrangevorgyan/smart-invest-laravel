<?php

namespace App\Models\Fragments\UserPlan;

use Carbon\Carbon;

trait Getters
{
    public function getSpentTimeViewMode(): string
    {
        $timeData = minutesToHumanMode(Carbon::now()->diffInMinutes($this->created_at));

        if ($timeData == null) return "Just Now";

        $spentTime = "";
        if ($timeData['y'] > 0) $spentTime .= $timeData['y'] .' y. ';
        if ($timeData['m'] > 0) $spentTime .= $timeData['m'] .' m. ';
        if ($timeData['d'] > 0) $spentTime .= $timeData['d'] .' d. ';
        if ($timeData['h'] > 0) $spentTime .= $timeData['h'] .' h. ';
        if ($timeData['i'] > 0) $spentTime .= $timeData['i'] .' min. ';

        return $spentTime;
    }

    public function getPendingProfit(): float
    {
        $profitPercentPerMinute = $this->profit_per_hour / 60;
        $notCollectedMinutes = Carbon::now()->diffInMinutes($this->last_collected_at ?? $this->created_at);

        return round($profitPercentPerMinute * $notCollectedMinutes, 2);
    }

    public function getPendingProfitAttribute(): float
    {
        return $this->getPendingProfit();
    }

    public function getPastTimeAttribute(): string
    {
        return $this->getSpentTimeViewMode();
    }

    public function getCanCollectAttribute(): bool
    {
        return $this->pending_profit > 0;
    }
}
