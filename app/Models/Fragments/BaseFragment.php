<?php

namespace App\Models\Fragments;

use App\Http\Filters\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

trait BaseFragment
{
    public function scopeFilterUsing(Builder $query, ?AbstractFilter $filter, string $method = 'handle', ...$args) : Builder
    {
        return $filter ? $filter->{$method}($query,...$args) : $query;
    }

    public static function findBy($column, $value)
    {
        return self::where($column, $value)->first();
    }

    public static function findOrFailBy($column, $value)
    {
        return self::where($column, $value)->firstOrFail();
    }

    public static function bulkDelete(array $ids): int
    {
        return static::destroy($ids);
    }

    public static function getTableName(): string
    {
        return (new static)->getTable();
    }

    public static function getDefaultAttributes(): array
    {
        return (new static)->getAttributes();
    }

    public static function getConstant(string $variable)
    {
        return constant('static::'. $variable);
    }
}
