<?php

namespace App\Models\Fragments\Replenishment;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait Relations
{
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
