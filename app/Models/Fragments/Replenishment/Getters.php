<?php

namespace App\Models\Fragments\Replenishment;

trait Getters
{
    public function getSystemNameAttribute(): string
    {
        return array_search($this->system_type, self::TYPES);
    }
}
