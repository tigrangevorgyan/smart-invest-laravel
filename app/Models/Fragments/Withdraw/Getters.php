<?php

namespace App\Models\Fragments\Withdraw;

trait Getters
{
    public function getSystemNameAttribute(): string
    {
        return array_search($this->system_type, self::TYPES);
    }

    public function getStatusDisplayNameAttribute(): string
    {
        return array_search($this->status, self::STATUSES);
    }
}
