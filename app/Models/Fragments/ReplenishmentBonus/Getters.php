<?php

namespace App\Models\Fragments\ReplenishmentBonus;

trait Getters
{
    public function getTypeDisplayNameAttribute(): string
    {
        switch ($this->type) {
            case self::TYPES['first_level']: return "Level 1";
            case self::TYPES['second_level']: return "Level 2";
        }

        return "Level 1";
    }
}
