<?php

namespace App\Models\Fragments\UserPaidContent;

use App\Models\PaidContent;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait Relations
{
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function paidContent(): BelongsTo
    {
        return $this->belongsTo(PaidContent::class);
    }
}
