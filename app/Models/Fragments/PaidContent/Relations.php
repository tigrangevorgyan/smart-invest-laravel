<?php

namespace App\Models\Fragments\PaidContent;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait Relations
{
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_paid_contents');
    }
}
