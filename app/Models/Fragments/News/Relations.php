<?php

namespace App\Models\Fragments\News;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait Relations
{
    public function attachment(): BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }
}
