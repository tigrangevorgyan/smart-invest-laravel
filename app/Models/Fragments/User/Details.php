<?php

namespace App\Models\Fragments\User;

use App\Models\PaidContent;
use App\Models\Plan;
use Illuminate\Support\Facades\Auth;

trait Details
{
    public function canSubscribe(Plan $plan): bool
    {
        return $this->shopping_balance >= $plan->cost;
    }

    public function canWithdraw(float $amount): bool
    {
        return $this->withdraw_balance >= $amount;
    }

    public function isAuthUser(): bool
    {
        return $this->id == Auth::id();
    }

    public function hasAccessToPaidContent(int $type): bool
    {
        return $this->currentPaidContents()->where('type', $type)->exists();
    }

    public function canUnblock(PaidContent $paidContent): bool
    {
        return $this->shopping_balance >= $paidContent->price;
    }
}
