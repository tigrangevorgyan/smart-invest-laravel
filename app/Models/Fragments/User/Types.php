<?php

namespace App\Models\Fragments\User;

trait Types
{
    public function isAdmin(): bool
    {
        return $this->type == self::TYPES['admin'];
    }

    public function isStandardUser(): bool
    {
        return $this->type == self::TYPES['user'];
    }
}
