<?php

namespace App\Models\Fragments\User;

use App\Models\User;

trait Getters
{
    public function getFullNameAttribute(): string
    {
        return $this->first_name .' ' .$this->last_name;
    }

    public static function getBySocial(int $type, string $socialId, string  $socialEmail): ?User
    {
        $query = User::where('email', $socialEmail);
        switch ($type) {
            case self::SIGN_UP_METHODS['facebook']:  $query->orWhere('facebook_id', $socialId); break;
            case self::SIGN_UP_METHODS['google']: $query->orWhere('google_id', $socialId); break;
        }

        return $query->first();
    }
}
