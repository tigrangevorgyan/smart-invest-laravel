<?php

namespace App\Models\Fragments\User;

use App\Models\PaidContent;
use App\Models\PaymentSystemInfo;
use App\Models\Plan;
use App\Models\Referral;
use App\Models\Replenishment;
use App\Models\User;
use App\Models\Withdraw;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait Relations
{
    public function plans(): BelongsToMany
    {
        return $this->belongsToMany(Plan::class, 'user_plans')->withPivot(
            'name', 'duration', 'cost', 'percent_per_day', 'profit_per_hour',
            'collected_profit', 'last_collected_at'
        );
    }

    public function replenishments(): HasMany
    {
        return $this->hasMany(Replenishment::class);
    }

    public function withdraws(): HasMany
    {
        return $this->hasMany(Withdraw::class);
    }

    public function inviter(): BelongsTo
    {
        return $this->belongsTo(self::class, 'inviter_id');
    }

    public function referrals(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'referrals', 'user_id', 'child_id')
            ->withPivot('level');
    }

    public function parents(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'referrals', 'child_id', 'user_id')
            ->withPivot('level');
    }

    public function paymentSystemInfos(): HasMany
    {
        return $this->hasMany(PaymentSystemInfo::class);
    }

    public function payeerSystemInfo(): HasOne
    {
        return $this->hasOne(PaymentSystemInfo::class)->where('type', PaymentSystemInfo::TYPES['payeer']);
    }

    public function paidContents(): BelongsToMany
    {
        return $this->belongsToMany(PaidContent::class, 'user_paid_contents')
            ->withPivot('expired_at');
    }

    public function currentPaidContents(): BelongsToMany
    {
        return $this->belongsToMany(PaidContent::class, 'user_paid_contents')
            ->wherePivot('expired_at', '>', Carbon::now())
            ->withPivot('expired_at');
    }
}
