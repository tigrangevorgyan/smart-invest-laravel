<?php

namespace App\Models\Fragments\Collect;

use App\Models\UserPlan;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait Relations
{
    public function userPlan(): BelongsTo
    {
        return $this->belongsTo(UserPlan::class);
    }
}
