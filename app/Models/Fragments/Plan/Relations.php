<?php

namespace App\Models\Fragments\Plan;

use App\Models\Attachment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait Relations
{
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_plans')->withPivot(
            'name', 'duration', 'cost', 'percent_per_day', 'profit_per_hour',
            'collected_profit', 'last_collected_at'
        );
    }

    public function currentUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_plans')
            ->where('ended_at', '>', Carbon::now())
            ->withPivot(
                'name', 'duration', 'cost', 'percent_per_day', 'profit_per_hour',
                'collected_profit', 'last_collected_at'
            );
    }

    public function finishedUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_plans')
            ->where('ended_at', '<', Carbon::now())
            ->withPivot(
                'name', 'duration', 'cost', 'percent_per_day', 'profit_per_hour',
                'collected_profit', 'last_collected_at'
            );
    }

    public function logo(): BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }
}
