<?php

namespace App\Models\Fragments\AppSetting;

trait Getters
{
    public function getDisplayTextAttribute(): string
    {
        switch ($this->key) {
            case self::SETTINGS['replenishment_bonus_from_1_level_ref']: return "Replenishment bonus from 1 level referrals";
            case self::SETTINGS['replenishment_bonus_from_2_level_ref']: return "Replenishment bonus from 2 level referrals";

            default: return '';
        }
    }
}
