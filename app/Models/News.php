<?php

namespace App\Models;

use App\Models\Fragments\News\Relations;

/**
 * @property Attachment attachment
 */
class News extends BaseModel
{
    use Relations;

    protected $fillable = [
        'title',
        'description',
        'attachment_id'
    ];
}
