<?php

namespace App\Models;

use App\Events\UserPlanCreated;
use App\Events\UserPlanUpdated;
use App\Models\Fragments\UserPlan\Details;
use App\Models\Fragments\UserPlan\Getters;
use App\Models\Fragments\UserPlan\Relations;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property Plan plan
 * @property Carbon created_at
 * @property int id
 * @property float profit_per_hour
 * @property float last_collected_at
 * @property float collected_profit
 * @property User user
 * @property float cost
 * @property float pending_profit
 */
class UserPlan extends BaseModel
{
    use Relations, Getters, Details;

    protected $fillable = [
        'name',
        'duration',
        'cost',
        'percent_per_day',
        'profit_per_hour',
        'finally_profit',
        'ended_at',
        'user_id',
        'plan_id',
        'collected_profit',
        'last_collected_at',
        'country'
    ];

    protected $dispatchesEvents = [
        'updated'   => UserPlanUpdated::class,
        'created'   => UserPlanCreated::class
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('availableForAuthUser', function (Builder $query) {
            if (!Auth::user()) return $query;

            if (Auth::user()->isStandardUser()) return $query->where('user_id', Auth::id());

            return $query;
        });
    }
}
