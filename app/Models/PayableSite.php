<?php

namespace App\Models;

class PayableSite extends BaseModel
{
    protected $fillable = [
        'site_name',
        'icon',
        'link',
        'using_cryptocurrency',
        'bonus'
    ];
}
