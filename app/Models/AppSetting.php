<?php

namespace App\Models;

use App\Models\Fragments\AppSetting\Getters;

/**
 * @property int key
 */
class AppSetting extends BaseModel
{
    use Getters;

    public const SETTINGS = [
        'replenishment_bonus_from_1_level_ref' => 1,
        'replenishment_bonus_from_2_level_ref' => 2
    ];

    public const VALUES = [
        'replenishment_bonus_from_1_level_ref'  => 5,   //percent
        'replenishment_bonus_from_2_level_ref'  => 1    //percent
    ];

    public const REFERRAL_SETTINGS = [
        'replenishment_bonus_from_1_level_ref' => 1,
        'replenishment_bonus_from_2_level_ref' => 2
    ];

    protected $fillable = [
        'key',
        'value'
    ];
}
