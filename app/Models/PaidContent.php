<?php

namespace App\Models;

use App\Models\Fragments\PaidContent\Relations;

/**
 * @property float price
 */
class PaidContent extends BaseModel
{
    use Relations;

    public const TYPES = [
        'payable_sites' => 1
    ];

    protected $fillable = [
        'type',
        'price',
        'duration'
    ];
}
