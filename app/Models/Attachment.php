<?php

namespace App\Models;

class Attachment extends BaseModel
{
    protected $fillable = [
        'name',
        'path'
    ];
}
