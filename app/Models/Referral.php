<?php

namespace App\Models;

use App\Models\Fragments\Referral\Relations;

class Referral extends BaseModel
{
    use Relations;

    protected $fillable = [
        'user_id',
        'child_id',
        'level'
    ];

    public const LEVELS = [
        'first'     => 1,
        'second'    => 2
    ];
}
