<?php

namespace App\Models;

use App\Events\ReplenishmentBonusCreated;
use App\Models\Fragments\ReplenishmentBonus\Getters;
use App\Models\Fragments\ReplenishmentBonus\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property int type
 * @property float amount
 * @property User user
 */
class ReplenishmentBonus extends BaseModel
{
    use Relations, Getters;

    protected $fillable = [
        'replenishment_id',
        'user_id',
        'type',
        'amount'
    ];

    public const TYPES = [
        'first_level'   => 1,
        'second_level'  => 2
    ];

    protected $dispatchesEvents = [
        'created' => ReplenishmentBonusCreated::class
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('availableForAuthUser', function (Builder $query) {
            if (!Auth::user()) return $query;

            if (Auth::user()->isStandardUser()) return $query->where('user_id', Auth::id());

            return $query;
        });
    }
}
