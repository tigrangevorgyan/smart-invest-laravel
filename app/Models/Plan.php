<?php

namespace App\Models;

use App\Models\Fragments\Plan\Relations;

/**
 * @property Attachment logo
 * @property float cost
 */
class Plan extends BaseModel
{
    use Relations;

    protected $fillable = [
        'name',
        'duration',
        'cost',
        'percent_per_day',
        'profit_per_hour',
        'finally_profit',
        'logo_id',
        'country'
    ];
}
