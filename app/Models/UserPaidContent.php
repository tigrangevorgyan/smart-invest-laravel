<?php

namespace App\Models;

use App\Events\UserPaidContentCreated;
use App\Models\Fragments\UserPaidContent\Relations;

/**
 * @property User user
 * @property float price
 */
class UserPaidContent extends BaseModel
{
    use Relations;

    protected $fillable = [
        'user_id',
        'paid_content_id',
        'price',
        'expired_at'
    ];

    protected $dispatchesEvents = [
        'created' => UserPaidContentCreated::class
    ];
}
