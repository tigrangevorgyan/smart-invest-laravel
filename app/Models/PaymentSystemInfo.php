<?php

namespace App\Models;

use App\Models\Fragments\PaymentSystemInfo\Relations;

class PaymentSystemInfo extends BaseModel
{
    use Relations;

    protected $fillable = [
        'user_id',
        'type',
        'account'
    ];

    public const TYPES = [
        'payeer' => 1
    ];
}
