<?php

namespace App\Models;

use App\Models\Fragments\BaseFragment;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use BaseFragment;

    public const ATTACHMENTS_PATH = 'images/';
}
