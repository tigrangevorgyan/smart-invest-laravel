<?php

namespace App\Models;

use App\Events\UserCreated;
use App\Events\UserCreating;
use App\Events\UserSaving;
use App\Models\Fragments\User\Details;
use App\Models\Fragments\User\Getters;
use App\Models\Fragments\User\Relations;
use App\Models\Fragments\User\Types;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Collection;

/**
 * @property int id
 * @property int type
 * @property double shopping_balance
 * @property double withdraw_balance
 * @property string ref_nickname
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property User inviter
 * @property int inviter_id
 * @property string transaction_code
 * @property string password
 * @property Collection paidContents
 * @property Collection referrals
 * @property Collection parents
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Notifiable, Authenticatable, Authorizable, CanResetPassword, Relations, Types, Details, Getters;

    public const TRANSACTION_CODE_LENGTH = 6;
    public const SIGN_UP_METHODS = [
        'registered'    => 1,
        'facebook'      => 2,
        'google'        => 3,
    ];

    //@TODO check transaction_code accessibility in email template, if it add to hidden fields list.

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'type',
        'password',
        'shopping_balance',
        'withdraw_balance',
        'ref_nickname',
        'google_id',
        'facebook_id',
        'sign_up_method',
        'inviter_id',
        'transaction_code'
    ];

    protected $hidden = [
        'remember_token',
        'password'
    ];

    public const TYPES = [
        'admin' => 1,
        'user'  => 2
    ];

    protected $attributes = [
        'type' => self::TYPES['user']
    ];

    protected $dispatchesEvents = [
        'creating'  => UserCreating::class,
        'created'   => UserCreated::class,
        'saving'    => UserSaving::class
    ];
}
