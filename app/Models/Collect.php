<?php

namespace App\Models;

use App\Events\CollectCreated;
use App\Models\Fragments\Collect\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property UserPlan userPlan
 * @property float amount
 */
class Collect extends BaseModel
{
    use Relations;

    protected $fillable = [
        'amount',
        'user_plan_id'
    ];

    protected $dispatchesEvents = [
        'created' => CollectCreated::class
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('availableForAuthUser', function (Builder $builder) {
            if (!Auth::user()) return $builder;

            if (Auth::user()->isStandardUser()) return $builder->whereHas('userPlan', function (Builder $builder) {
                $builder->where('user_id', Auth::id());
            });

            return $builder;
        });
    }
}
