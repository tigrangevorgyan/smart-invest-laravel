<?php

namespace App\Models;

use App\Events\WithdrawCreated;
use App\Models\Fragments\Withdraw\Getters;
use App\Models\Fragments\Withdraw\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property User user
 * @property float amount
 * @property float commission
 * @property int system_type
 * @property int status
 */
class Withdraw extends BaseModel
{
    use Relations, Getters;

    public const PAYEER_COMMISSION = 0.5;
    public const MIN = 1;
    public const MAX = 10000;

    protected $fillable = [
        'user_id',
        'amount',
        'system_number',
        'system_type',
        'status',
        'commission'
    ];

    public const TYPES = [
        'payeer' => 1
    ];

    public const STATUSES = [
        'declined'  => 0,
        'pending'   => 1,
        'accepted'  => 2
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('availableForAuthUser', function (Builder $query) {
            if (!Auth::user()) return $query;

            if (Auth::user()->isStandardUser()) return $query->where('user_id', Auth::id());

            return $query;
        });
    }

    protected $dispatchesEvents = [
        'created' => WithdrawCreated::class
    ];
}
