<?php

namespace App\Events;

use App\Models\Withdraw;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WithdrawCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $withdraw;

    public function __construct(Withdraw $withdraw)
    {
        $this->withdraw = $withdraw;
    }
}
