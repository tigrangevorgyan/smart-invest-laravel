<?php

namespace App\Events;

use App\Models\UserPaidContent;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPaidContentCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userPaidContent;

    public function __construct(UserPaidContent $userPaidContent)
    {
        $this->userPaidContent = $userPaidContent;
    }
}
