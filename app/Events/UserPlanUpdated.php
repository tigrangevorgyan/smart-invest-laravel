<?php

namespace App\Events;

use App\Models\UserPlan;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPlanUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userPlan;

    public function __construct(UserPlan $userPlan)
    {
        $this->userPlan = $userPlan;
    }
}
