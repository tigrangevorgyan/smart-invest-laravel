<?php

namespace App\Events;

use App\Models\Replenishment;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReplenishmentCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $replenishment;

    public function __construct(Replenishment $replenishment)
    {
        $this->replenishment = $replenishment;
    }
}
