<?php

namespace App\Events;

use App\Models\Collect;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CollectCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $collect;

    public function __construct(Collect $collect)
    {
        $this->collect = $collect;
    }
}
