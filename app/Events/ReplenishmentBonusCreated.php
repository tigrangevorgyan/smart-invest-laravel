<?php

namespace App\Events;

use App\Models\ReplenishmentBonus;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReplenishmentBonusCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $replenishmentBonus;

    public function __construct(ReplenishmentBonus $replenishmentBonus)
    {
        $this->replenishmentBonus = $replenishmentBonus;
    }
}
