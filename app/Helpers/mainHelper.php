<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

function getClassName(Model $model): string
{
    $parts = explode('\\', get_class($model));

    return end($parts);
}

function modelClass(string $model): string
{
    return '\\App\\Models\\' . ucfirst(Str::camel($model));
}

function pivotClass(string $model): string
{
    return '\\App\\Models\\Pivots\\' . ucfirst(Str::camel($model));
}

function array_equal($array1, $array2): bool
{
    return !array_diff($array1, $array2) && !array_diff($array2, $array1);
}

function getOrderTypes(): array
{
    return ['asc', 'desc'];
}

function getProfitPerHour(float $cost, float $percentPerDay): float
{
    return round($cost * $percentPerDay / 100 / 24, 2);
}

function getFinallyProfit(int $duration, float $cost, float $percentPerDay): float
{
    return round($duration * $cost * $percentPerDay / 100);
}

function minutesToHumanMode($minutes): ?array
{
    if (!$minutes || !is_integer($minutes)) return null;

    $date1 = date_create(date('Y-m-d H:i:s'));
    $date2 = date_create(date('Y-m-d H:i:s', strtotime("$minutes minute")));

    $diff = date_diff($date2, $date1);

    return [
        'y' => $diff->y,
        'm' => $diff->m,
        'd' => $diff->d,
        'h' => $diff->h,
        'i' => $diff->i
    ];
}

function generateTransactionCode(): string
{
    return Str::random(User::TRANSACTION_CODE_LENGTH);
}

function generateRefLink(User $user): string
{
    return config('app.url') ."/login-page?ref=$user->ref_nickname";
}

function getPercent(float $mainNumber, float $percent): float
{
    return round($mainNumber * $percent / 100, 2);
}
