<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        CustomAuthorizationException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
        HttpException::class,
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render($request, Throwable $exception)
    {
        if ($request->expectsJson()) {
            $exception = $this->prepareException($exception);
            if ($exception instanceof CustomAuthorizationException) {
                return $this->customResponse(
                    ['errorMessage' => json_decode($exception->getMessage())],
                    $exception->getStatusCode()
                );
            }
            if ($exception instanceof AccessDeniedHttpException){
                return $this->customResponse(
                    ['errorMessage' => $exception->getMessage()],
                    $exception->getPrevious()->getCode()
                );
            }
            if ($exception instanceof NotFoundHttpException
                or
                $exception instanceof HttpException
                or
                $exception instanceof ModelNotFoundException
                or
                $exception instanceof HttpResponseException) {
                return $this->customResponse(
                    ['errorMessage' => $exception->getMessage()],
                    $exception->getStatusCode()
                );
            }
            if ($exception instanceof AuthenticationException
                or
                $exception instanceof ValidationException) {
                return parent::render($request, $exception);
            }

            if (!config('app.debug')){
                return $this->customResponse(
                    ['errorMessage' => 'Sorry, something went wrong'],
                    500
                );
            }

            return $this->customResponse([
                'errorMessage' => $exception->getMessage(),
                'errorClass'   => get_class($exception),
                'file'         => $exception->getFile(),
                'line'         => $exception->getLine()
            ], 500);
        }
        else {
            return parent::render($request, $exception);
        }
    }

    private function customResponse(array $errorMessage, int $statusCode)
    {
        return response()->json(array_merge($errorMessage, [
            // add custom lines here
        ]), $statusCode);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
