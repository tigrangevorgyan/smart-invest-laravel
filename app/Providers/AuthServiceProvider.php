<?php

namespace App\Providers;

use App\Models\Collect;
use App\Models\PaidContent;
use App\Models\Plan;
use App\Models\User;
use App\Models\Withdraw;
use App\Policies\CollectPolicy;
use App\Policies\PaidContentPolicy;
use App\Policies\PlanPolicy;
use App\Policies\UserPolicy;
use App\Policies\WithdrawPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Plan::class         => PlanPolicy::class,
        Collect::class      => CollectPolicy::class,
        Withdraw::class     => WithdrawPolicy::class,
        User::class         => UserPolicy::class,
        PaidContent::class  => PaidContentPolicy::class
    ];

    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
