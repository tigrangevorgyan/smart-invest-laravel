<?php

namespace App\Providers;

use App\Events\CollectCreated;
use App\Events\ReplenishmentBonusCreated;
use App\Events\ReplenishmentCreated;
use App\Events\UserCreated;
use App\Events\UserCreating;
use App\Events\UserPaidContentCreated;
use App\Events\UserPlanCreated;
use App\Events\UserPlanUpdated;
use App\Events\UserSaving;
use App\Events\WithdrawCreated;
use App\Listeners\CollectCreated\UpdateUserPlan;
use App\Listeners\ReplenishmentBonusCreated\AddParentUsersBalance;
use App\Listeners\ReplenishmentCreated\AddReplenishmentBonus;
use App\Listeners\ReplenishmentCreated\AddUserShoppingBalance;
use App\Listeners\UserCreated\AttachReferrals;
use App\Listeners\UserCreating\SetRefNickName;
use App\Listeners\UserCreating\SetTransactionCode;
use App\Listeners\UserPlanCreated\UpdateUserBalance;
use App\Listeners\UserPlanUpdated\ReCalculateUserBalances;
use App\Listeners\UserSaving\BcryptPassword;
use App\Listeners\UserSaving\SendNewTransactionCodeEmail;
use App\Listeners\WithdrawCreated\DecrementUserWithdrawBalance;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UserPlanUpdated::class => [
            ReCalculateUserBalances::class
        ],
        UserPlanCreated::class => [
            UpdateUserBalance::class
        ],
        CollectCreated::class => [
            UpdateUserPlan::class
        ],
        UserCreating::class => [
            SetRefNickName::class,
            SetTransactionCode::class
        ],
        UserCreated::class => [
            AttachReferrals::class
        ],
        ReplenishmentCreated::class => [
            AddUserShoppingBalance::class,
            AddReplenishmentBonus::class
        ],
        WithdrawCreated::class => [
            DecrementUserWithdrawBalance::class
        ],
        UserSaving::class => [
            BcryptPassword::class,
            SendNewTransactionCodeEmail::class
        ],
        UserPaidContentCreated::class => [
            \App\Listeners\UserPaidContentCreated\UpdateUserBalance::class
        ],
        ReplenishmentBonusCreated::class => [
            AddParentUsersBalance::class
        ]
    ];

    public function boot()
    {
        parent::boot();

        //
    }
}
