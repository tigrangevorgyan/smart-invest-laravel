<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->customValidators();
    }

    private function customValidators()
    {
        Validator::extend('exists_on_model', 'App\Http\Validators\BaseValidator@existsOnModel');
    }
}
