<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\BaseRequest;

class OfficeRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
