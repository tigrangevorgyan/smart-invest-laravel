<?php

namespace App\Http\Requests;

use App\Models\BaseModel;

abstract class DataPersistRequest extends BaseRequest
{
    public $fileNames = [];
    private $storePath = BaseModel::ATTACHMENTS_PATH;
    private $defaultExceptedKeys = [
        'created_at',
        'updated_at'
    ];

    public abstract function persist();

    public function storeFilesIfExists(): array
    {
        $filePaths = [];
        foreach ($this->fileNames as $fileName => $storeName){
            if (is_int($fileName)) {
                $fileName = $storeName;
            }
            if ($this->hasFile($fileName)) {
                $path = $this->file($fileName)->store($this->storePath);
                $filePaths[$storeName] = pathinfo($path)['basename'];
            }
        }

        return $filePaths;
    }

    protected function getMergingData(): array
    {
        return [];
    }

    protected function getExceptedKeys(): array
    {
        return [
            'created_at',
            'updated_at',
        ];
    }

    protected function getProcessedData(bool $ignoreDefaultExceptedKeys = false): array
    {
        return array_merge($this->except($ignoreDefaultExceptedKeys ?
            $this->getExceptedKeys() : array_merge($this->defaultExceptedKeys, $this->getExceptedKeys())),
            $this->storeFilesIfExists(),
            $this->getMergingData()
        );
    }
}

