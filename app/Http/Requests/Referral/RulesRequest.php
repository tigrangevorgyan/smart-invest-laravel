<?php

namespace App\Http\Requests\Referral;

use App\Http\Requests\BaseRequest;
use App\Models\AppSetting;
use Illuminate\Support\Collection;

class RulesRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function getReferralAppSettings(): Collection
    {
        return AppSetting::whereIn('key', AppSetting::REFERRAL_SETTINGS)
            ->get();
    }
}
