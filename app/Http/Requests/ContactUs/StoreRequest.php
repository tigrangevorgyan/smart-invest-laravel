<?php

namespace App\Http\Requests\ContactUs;

use App\Http\Requests\DataPersistRequest;
use App\Mail\ContactUsEmail;
use Illuminate\Support\Facades\Mail;

/**
 * @property string email
 * @property string subject
 * @property string text
 */
class StoreRequest extends DataPersistRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return [
            'subject'   => 'required',
            'email'     => 'required',
            'text'       => 'required'
        ];
    }

    public function persist(): self
    {
        Mail::send(new ContactUsEmail($this->subject, $this->text, $this->email));

        return $this;
    }

    public function getMessage(): array
    {
        return [
            'success' => true
        ];
    }
}
