<?php

namespace App\Http\Requests\Transaction\Bonus;

use App\Http\Requests\BaseRequest;

class ReplenishmentBonusRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return array_merge($this->getCommonRulesForListing(), [
            //
        ]);
    }
}
