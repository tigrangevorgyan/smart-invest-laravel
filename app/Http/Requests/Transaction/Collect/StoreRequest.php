<?php

namespace App\Http\Requests\Collect;

use App\Http\Requests\DataPersistRequest;

use App\Models\Collect;
use App\Models\UserPlan;
use Illuminate\Support\Facades\Auth;

/**
 * @property int user_plan_id
 */
class StoreRequest extends DataPersistRequest
{
    private $userPlan;
    private $pendingProfit = 0;
    protected $forbiddenMessages = [
        'store' => "Not enough funds to collect."
    ];

    public function authorizationRules(): array
    {
        return [
            'store' => Auth::user()->can('store', [
                Collect::class,
                $this->userPlan
            ])
        ];
    }

    protected function beforeAuthorization(): void
    {
        $this->userPlan = UserPlan::find($this->user_plan_id);
    }

    public function rules()
    {
        return [
            'user_plan_id' => 'required|exists_on_model:userPlan,id'
        ];
    }

    public function persist(): self
    {
        Collect::create([
            'user_plan_id'  => $this->userPlan->id,
            'amount'        => $this->pendingProfit = $this->userPlan->getPendingProfit()
        ]);

        return $this;
    }

    public function getCollectedData(): array
    {
        return [
            'success_collected' => $this->pendingProfit > 0,
            'amount'            => $this->pendingProfit,
            'withdraw_balance'  => round(Auth::user()->fresh()->withdraw_balance, 2),
            'id'                => $this->userPlan->id,
        ];
    }
}
