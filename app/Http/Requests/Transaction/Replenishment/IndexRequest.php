<?php

namespace App\Http\Requests\Transaction\Replenishment;

use App\Http\Requests\BaseRequest;

class IndexRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return array_merge($this->getCommonRulesForListing(), [
            //
        ]);
    }
}
