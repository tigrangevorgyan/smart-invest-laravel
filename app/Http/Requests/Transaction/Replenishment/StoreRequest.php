<?php

namespace App\Http\Requests\Transaction\Replenishment;

use App\Http\Requests\DataPersistRequest;

use App\Models\Replenishment;
use Illuminate\Support\Facades\Auth;

/**
 * @property float amount
 */
class StoreRequest extends DataPersistRequest
{
    private $replenishment;

    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:0.1|max:10000'
        ];
    }

    public function persist(): self
    {
        $this->replenishment = Replenishment::create([
            'user_id'       => Auth::id(),
            'commission'    => $commission = $this->amount * Replenishment::PAYEER_COMMISSION / 100,
            'amount'        => $this->amount - $commission,
            'system_type'   => Replenishment::TYPES['payeer'],
            'system_number' => 'P' .rand(1000000000, 9999999999)
        ]);

        return $this;
    }

    public function getReplenishmentData(): array
    {
        return [
            'amount' => $this->replenishment->amount,
        ];
    }
}
