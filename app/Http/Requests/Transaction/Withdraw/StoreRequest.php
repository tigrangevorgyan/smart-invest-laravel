<?php

namespace App\Http\Requests\Transaction\Withdraw;

use App\Http\Requests\DataPersistRequest;

use App\Models\Withdraw;
use Illuminate\Support\Facades\Auth;

/**
 * @property float amount
 */
class StoreRequest extends DataPersistRequest
{
    private $withdraw;

    protected $forbiddenMessages = [
        'store' => 'You don`t have enough money for withdraw.',
    ];

    public function authorizationRules(): array
    {
        return [
            'store' => Auth::user()->can('store', [
                Withdraw::class, $this->amount
            ])
        ];
    }

    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:1|max:10000'
        ];
    }

    public function persist(): self
    {
        $this->withdraw = Withdraw::create([
            'user_id'       => Auth::id(),
            'commission'    => $commission = $this->amount * Withdraw::PAYEER_COMMISSION / 100,
            'amount'        => $this->amount - $commission,
            'system_type'   => Withdraw::TYPES['payeer'],
            'system_number' => 'P' .rand(1000000000, 9999999999),
            'status'        => Withdraw::STATUSES['accepted']
        ]);

        return $this;
    }

    public function getWithdrawData(): array
    {
        return [
            'amount'            => $this->withdraw->amount,
            'withdraw_balance'  => round(Auth::user()->fresh()->withdraw_balance, 2)
        ];
    }
}
