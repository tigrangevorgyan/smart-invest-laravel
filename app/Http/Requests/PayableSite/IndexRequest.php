<?php

namespace App\Http\Requests\PayableSite;

use App\Http\Requests\BaseRequest;

class IndexRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return array_merge($this->getCommonRulesForListing(), [
            //
        ]);
    }
}
