<?php

namespace App\Http\Requests\UserPlan;

use App\Http\Requests\BaseRequest;

class MyPlansRequest extends BaseRequest
{
    public function authorizationRules(): array
    {
        return [
            //
        ];
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
