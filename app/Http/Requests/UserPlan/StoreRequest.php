<?php

namespace App\Http\Requests\UserPlan;

use App\Http\Requests\DataPersistRequest;
use App\Models\Plan;
use App\Models\UserPlan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * @property int plan_id
 */
class StoreRequest extends DataPersistRequest
{
    private $plan;
    private $userPlan;
    protected $forbiddenMessages = [
        'store' => "You need an extra $%1.2f in your shopping balance to complete your subscription."
    ];

    public function authorizationRules(): array
    {
        return [
            'store' => Auth::user()->can('subscribe', $this->plan)
        ];
    }

    protected function beforeAuthorization(): void
    {
        $this->plan = Plan::find($this->plan_id);

        $this->manageForbiddenMessages();
    }

    private function manageForbiddenMessages()
    {
        $this->forbiddenMessages['store'] = sprintf($this->forbiddenMessages['store'],
            round($this->plan->cost - Auth::user()->fresh()->shopping_balance, 2)
        );
    }

    public function rules()
    {
        return [
            'plan_id' => 'required|exists_on_model:plan,id'
        ];
    }

    public function persist(): self
    {
        $this->createUserPlan();

        return $this;
    }

    private function createUserPlan(): void
    {
        if (true) {
            $this->userPlan = UserPlan::create([
                'user_id'           => Auth::id(),
                'plan_id'           => $this->plan_id,
                'name'              => $this->plan->name,
                'country'           => $this->plan->country,
                'duration'          => $this->plan->duration,
                'cost'              => $this->plan->cost,
                'percent_per_day'   => $this->plan->percent_per_day,
                'profit_per_hour'   => $this->plan->profit_per_hour,
                'finally_profit'    => $this->plan->finally_profit,
                'collected_profit'  => 0,
                'ended_at'          => Carbon::now()->addDays($this->plan->duration)
            ]);
        }
    }

    public function getUserPlanInfo(): array
    {
        return [
            'success_purchased' => $this->userPlan != null,
            'plan_name'         => $this->userPlan->name ?? null,
            'shopping_balance'  => round(Auth::user()->fresh()->shopping_balance, 2)
        ];
    }
}
