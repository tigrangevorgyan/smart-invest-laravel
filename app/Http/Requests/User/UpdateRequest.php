<?php

namespace App\Http\Requests\User;

use App\Http\Requests\DataPersistRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * @property string oldPassword
 * @property User user
 */
class UpdateRequest extends DataPersistRequest
{
    protected $forbiddenMessages = [
        'update'            => 'You can update only own data.',
        'changePassword'    => 'Old password is incorrect.'
    ];

    public function authorizationRules(): array
    {
        return [
            'update'            => Auth::user()->can('update', $this->user),
            'changePassword'    => (!$this->has('password') || Hash::check($this->oldPassword, $this->user->password))
        ];
    }

    public function rules()
    {
        return [
            'first_name'    => 'sometimes|required|max:90',
            'last_name'     => 'sometimes|required|max:90',
            'password'      => 'sometimes|required|max:20|min:6',
            'oldPassword'   => 'required_with:password'
        ];
    }

    public function persist(): self
    {
        $this->user->update(
            $this->getProcessedData()
        );

        return $this;
    }

    protected function getExceptedKeys(): array
    {
        return [
            'inviter_id',
            'ref_nickname',
            'transaction_code',
            'type',
            'email',
            'shopping_balance',
            'withdraw_balance'
        ];
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
