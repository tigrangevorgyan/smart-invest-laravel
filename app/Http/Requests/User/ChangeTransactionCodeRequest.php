<?php

namespace App\Http\Requests\User;

use App\Http\Requests\DataPersistRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @property User user
 */
class ChangeTransactionCodeRequest extends DataPersistRequest
{
    protected $forbiddenMessages = [
        'update' => 'You can update only own data.',
    ];

    public function authorizationRules(): array
    {
        return [
            'update' => Auth::user()->can('update', $this->user)
        ];
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(): self
    {
        $this->user->update([
            'transaction_code' => generateTransactionCode()
        ]);

        return $this;
    }
}
