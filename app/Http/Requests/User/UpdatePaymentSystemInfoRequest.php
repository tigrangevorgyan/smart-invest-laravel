<?php

namespace App\Http\Requests\User;

use App\Http\Requests\DataPersistRequest;
use App\Models\PaymentSystemInfo;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * @property User user
 * @property int type
 * @property string account
 */
class UpdatePaymentSystemInfoRequest extends DataPersistRequest
{
    protected $forbiddenMessages = [
        'update' => 'You can update only own data.',
    ];

    public function authorizationRules(): array
    {
        return [
            'update' => Auth::user()->can('update', $this->user)
        ];
    }

    public function rules()
    {
        return [
            'type'              => ['required', Rule::in(PaymentSystemInfo::TYPES)],
            'account'           => 'nullable|max:90',
            'transaction_code'  => ['required', Rule::exists('users', 'transaction_code')->where('id', Auth::id())]
        ];
    }

    public function persist(): self
    {
        PaymentSystemInfo::updateOrCreate([
            'user_id'   => $this->user->id,
            'type'      => $this->type
        ], [
            'account'   => $this->account
        ]);

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
