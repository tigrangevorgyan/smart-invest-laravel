<?php

namespace App\Http\Requests\PaidContent;

use App\Http\Requests\DataPersistRequest;

use App\Models\PaidContent;
use App\Models\UserPaidContent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * @property int type
 */
class StoreRequest extends DataPersistRequest
{
    private $paidContent;
    private $userPaidContent = null;

    protected $forbiddenMessages = [
        'store' => "Not enough funds to unblock information."
    ];

    protected function beforeAuthorization(): void
    {
        $this->paidContent = PaidContent::findBy('type', $this->type);
    }

    public function authorizationRules(): array
    {
        return [
            'store' => Auth::user()->can('unblock', [
                PaidContent::class,
                $this->paidContent
            ])
        ];
    }

    public function rules()
    {
        return [
            'type' => ['required', Rule::in(PaidContent::TYPES)]
        ];
    }

    public function persist(): self
    {
        $this->userPaidContent = UserPaidContent::create([
            'user_id'           => Auth::id(),
            'paid_content_id'   => $this->paidContent->id,
            'price'             => $this->paidContent->price,
            'expired_at'        => Carbon::now()->addDays($this->paidContent->duration)
        ]);

        return $this;
    }

    public function getData(): array
    {
        return [
            'success' => $this->userPaidContent != null,
        ];
    }
}
