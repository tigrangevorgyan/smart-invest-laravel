<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class ReplenishmentTransformer extends BaseTransformer
{
    public function simpleTransform(Model $replenishment): array
    {
        return [
            'id'            => $replenishment->id,
            'amount'        => $replenishment->amount,
            'system_number' => $replenishment->system_number,
            'system_name'   => $replenishment->system_name,
            'system_type'   => $replenishment->system_type,
            'created_at'    => $this->dateTransform($replenishment->created_at)
        ];
    }
}
