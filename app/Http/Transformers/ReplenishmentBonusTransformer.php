<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class ReplenishmentBonusTransformer extends BaseTransformer
{
    public function simpleTransform(Model $replenishmentBonus): array
    {
        return [
            'id'                => $replenishmentBonus->id,
            'amount'            => $replenishmentBonus->amount,
            'type_display_name' => $replenishmentBonus->type_display_name,
            'created_at'        => $this->dateTransform($replenishmentBonus->created_at)
        ];
    }
}
