<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class WithdrawTransformer extends BaseTransformer
{
    public function simpleTransform(Model $withdraw): array
    {
        return [
            'id'                    => $withdraw->id,
            'amount'                => $withdraw->amount,
            'system_number'         => $withdraw->system_number,
            'system_name'           => $withdraw->system_name,
            'system_type'           => $withdraw->system_type,
            'status'                => $withdraw->status,
            'status_display_name'   => $withdraw->status_display_name,
            'created_at'            => $this->dateTransform($withdraw->created_at)
        ];
    }
}
