<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class UserTransformer extends BaseTransformer
{
    public function simpleTransform(Model $user): array
    {
        return [
            'id'                => $user->id,
            'first_name'        => $user->first_name,
            'last_name'         => $user->last_name,
            'type'              => $user->type,
            'email'             => $user->email,
            'shopping_balance'  => $user->shopping_balance,
            'withdraw_balance'  => $user->withdraw_balance,
            'ref_nickname'      => $user->ref_nickname
        ];
    }
}
