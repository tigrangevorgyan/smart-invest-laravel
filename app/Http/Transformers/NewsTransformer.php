<?php

namespace App\Http\Transformers;

use App\Models\News;
use Illuminate\Database\Eloquent\Model;

class NewsTransformer extends BaseTransformer
{
    public function detailedTransform(News $news): array
    {
        return array_merge($this->simpleTransform($news), [
            'attachment' => AttachmentTransformer::simple($news->attachment)
        ]);
    }

    public function simpleTransform(Model $news): array
    {
        return [
            'id'            => $news->id,
            'title'         => $news->title,
            'description'   => $news->description,
            'created_at'    => $this->dateTransform($news->created_at)
        ];
    }
}
