<?php

namespace App\Http\Transformers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

abstract class BaseTransformer
{
    public static function __callStatic($name, $arguments): array
    {
        $instance = new static();

        if (method_exists($instance, 'transform' . ucfirst($name))) return $instance->{'transform' . ucfirst($name)}(...$arguments);

        return $instance->{$name . 'Transform'}(...$arguments);
    }

    public function transformUniversal($providedData, string $method = 'simpleTransform')
    {
        if ($providedData instanceof Collection) return $this->transformCollection($providedData, $method);

        return $this->transformPagination($providedData, $method);
    }

    public function transformPagination($paginator, string $method = 'simpleTransform', ...$args) : array
    {
        return [
            'total'         => method_exists($paginator, 'total') ? $paginator->total() : null,
            'hasMorePages'  => method_exists($paginator, 'hasMorePages') ? $paginator->hasMorePages() : null,
            'items'         => $this->transformArray($paginator->items(), $method, ...$args)
        ];

    }

    public function transformCollection(\ArrayAccess $items, string $method = 'simpleTransform', ...$args) : array
    {
        return $items->map(function ($item) use ($method,$args) {
            return $this->{$method}($item, ...$args);
        })->toArray();
    }

    public function transformArray(array $items, string $method = 'simpleTransform', ...$args) : array
    {
        return array_map(function($item) use ($method,$args) {
            return $this->{$method}($item, ...$args);
        }, $items);
    }

    abstract public function simpleTransform(Model $item): array;

    protected function dateTransform(string $dateTime): string
    {
        return Carbon::parse($dateTime)->toDateTimeString();
    }
}
