<?php

namespace App\Http\Transformers;

use App\Models\Plan;
use Illuminate\Database\Eloquent\Model;

class AttachmentTransformer extends BaseTransformer
{
    public function simpleTransform(Model $attachment): array
    {
        return [
            'id'    => $attachment->id,
            'name'  => $attachment->name,
            'path'  => $attachment->path
        ];
    }
}
