<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class PayableSiteTransformer extends BaseTransformer
{
    public function simpleTransform(Model $payableSite): array
    {
        return [
            'id'        => $payableSite->id,
            'name'      => $payableSite->site_name,
            'link'      => $payableSite->link,
            'crypto'    => $payableSite->using_cryptocurrency
        ];
    }
}
