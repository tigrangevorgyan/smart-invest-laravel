<?php

namespace App\Http\Transformers;

use Illuminate\Database\Eloquent\Model;

class CollectTransformer extends BaseTransformer
{
    public function simpleTransform(Model $collect): array
    {
        return [
            'id'            => $collect->id,
            'amount'        => $collect->amount,
            'created_at'    => $this->dateTransform($collect->created_at)
        ];
    }
}
