<?php

namespace App\Http\Transformers;

use App\Models\Plan;
use App\Models\UserPlan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserPlanTransformer extends BaseTransformer
{
    public function detailedTransform(UserPlan $userPlan): array
    {
        return array_merge($this->simpleTransform($userPlan), [
            'past_time'         => $userPlan->getSpentTimeViewMode(),
            'pending_profit'    => $pendingProfit = $userPlan->getPendingProfit(),
            'can_collect'       => $pendingProfit > 0,
            'logo'              => AttachmentTransformer::simple($userPlan->plan->logo)
        ]);
    }

    public function simpleTransform(Model $plan): array
    {
        return [
            'id'                => $plan->id,
            'name'              => $plan->name,
            'duration'          => $plan->duration,
            'cost'              => $plan->cost,
            'percent_per_day'   => $plan->percent_per_day,
            'profit_per_hour'   => $plan->profit_per_hour,
            'finally_profit'    => $plan->finally_profit,
            'collected_profit'  => $plan->collected_profit,
            'created_at'        => $this->dateTransform($plan->created_at),
            'ended_at'          => $this->dateTransform($plan->ended_at)
        ];
    }
}
