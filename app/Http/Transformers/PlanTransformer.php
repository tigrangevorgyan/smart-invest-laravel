<?php

namespace App\Http\Transformers;

use App\Models\Plan;
use Illuminate\Database\Eloquent\Model;

class PlanTransformer extends BaseTransformer
{
    public function detailedTransform(Plan $plan): array
    {
        return array_merge($this->simpleTransform($plan), [
            'logo' => AttachmentTransformer::simple($plan->logo)
        ]);
    }

    public function simpleTransform(Model $plan): array
    {
        return [
            'id'                => $plan->id,
            'name'              => $plan->name,
            'duration'          => $plan->duration,
            'cost'              => $plan->cost,
            'percent_per_day'   => $plan->percent_per_day,
            'profit_per_hour'   => $plan->profit_per_hour,
            'finally_profit'    => $plan->finally_profit
        ];
    }
}
