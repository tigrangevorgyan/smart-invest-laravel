<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        $messages = [
            'exists_on_model' => 'The given '.request()->ref_nickname.' referral name was invalid.',
        ];
        return Validator::make($data, [
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'      => ['required', 'string', 'min:6', 'confirmed'],
            'ref_nickname'  => ['string', 'exists_on_model:user,ref_nickname']
        ], $messages);
    }

    protected function create(array $data)
    {
        if (isset($data['ref_nickname']) && !is_null($data['ref_nickname'])) {
            $parentUser = User::findBy('ref_nickname', $data['ref_nickname']);
        }

        return User::create([
            'first_name'    => $data['first_name'],
            'last_name'     => $data['last_name'],
            'email'         => $data['email'],
            'password'      => Hash::make($data['password']),
            'inviter_id'    => $parentUser->id ?? null
        ]);
    }
}
