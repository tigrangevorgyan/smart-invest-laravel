<?php

namespace App\Http\Controllers;

use App\Http\DataProviders\Welcome\WelcomePageDataProvider;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index(): View
    {
        return view('home');
    }

    public function welcomePage(WelcomePageDataProvider $provider): View
    {
        $data = $provider->getData();

        return view('welcome', [
            'plans' => $data['plans'],
            'news'  => $data['news']
        ]);
    }
}
