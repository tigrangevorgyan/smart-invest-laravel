<?php

namespace App\Http\Controllers\Api\V1\PaidContent;

use App\Http\Requests\PaidContent\StoreRequest;

class PaidContentController
{
    public function store(StoreRequest $request): array
    {
        $data = $request->persist()->getData();

        return [
            'success' => $data['success']
        ];
    }
}
