<?php

namespace App\Http\Controllers\Api\V1\UserPlan;

use App\Http\DataProviders\UserPlan\MyPlansDataProvider;
use App\Http\Requests\UserPlan\MyPlansRequest;
use App\Http\Requests\UserPlan\StoreRequest;
use Illuminate\View\View;

class UserPlanController
{
    public function store(StoreRequest $request): array
    {
        return $request->persist()->getUserPlanInfo();
    }

    public function myPlans(MyPlansRequest $request, MyPlansDataProvider $provider): View
    {
        return view('app.user.myPlans', [
            'myPlans' => $provider->getData()
        ]);
    }
}
