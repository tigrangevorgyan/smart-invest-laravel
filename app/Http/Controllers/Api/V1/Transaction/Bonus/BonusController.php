<?php

namespace App\Http\Controllers\Api\V1\Transaction\Bonus;

use App\Http\DataProviders\Bonus\ReplenishmentBonusDataProvider;
use App\Http\Requests\Transaction\Bonus\ReplenishmentBonusRequest;
use App\Http\Transformers\ReplenishmentBonusTransformer;
use Illuminate\View\View;

class BonusController
{
    public function replenishmentBonus(ReplenishmentBonusRequest $request, ReplenishmentBonusDataProvider $provider): View
    {
        $replenishmentBonuses = ReplenishmentBonusTransformer::universal(
            $provider->getData()
        );

        return view('app.transaction.replenishment-bonus-list', [
            'replenishmentBonuses' => $replenishmentBonuses['items']
        ]);
    }
}
