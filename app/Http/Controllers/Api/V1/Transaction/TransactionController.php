<?php

namespace App\Http\Controllers\Api\V1\Transaction;

use Illuminate\View\View;

class TransactionController
{
    public function index()
    {
        dd(12121);
    }

    public function replenishmentPage(): View
    {
        return view('app.transaction.replenishment', [
            //
        ]);
    }

    public function withdrawPage(): View
    {
        return view('app.transaction.withdraw', [
            //
        ]);
    }
}
