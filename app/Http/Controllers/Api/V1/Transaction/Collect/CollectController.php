<?php

namespace App\Http\Controllers\Api\V1\Transaction\Collect;

use App\Http\DataProviders\Collect\IndexDataProvider;
use App\Http\Requests\Transaction\Collect\IndexRequest;
use App\Http\Requests\Collect\StoreRequest;
use App\Http\Transformers\CollectTransformer;
use Illuminate\View\View;

class CollectController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        $collects = CollectTransformer::universal(
            $provider->getData()
        );

        return view('app.transaction.collect-list', [
            'collects' => $collects['items']
        ]);
    }

    public function store(StoreRequest $request): array
    {
        return $request->persist()->getCollectedData();
    }
}
