<?php

namespace App\Http\Controllers\Api\V1\Transaction\Withdraw;

use App\Http\DataProviders\Withdraw\IndexDataProvider;
use App\Http\Requests\Transaction\Withdraw\IndexRequest;
use App\Http\Requests\Transaction\Withdraw\StoreRequest;
use App\Http\Transformers\WithdrawTransformer;
use Illuminate\View\View;

class WithdrawController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        $withdraws = WithdrawTransformer::universal(
            $provider->getData()
        );

        return view('app.transaction.withdraw-list', [
            'withdraws' => $withdraws['items']
        ]);
    }

    public function store(StoreRequest $request): array
    {
        return $request->persist()->getWithdrawData();
    }
}
