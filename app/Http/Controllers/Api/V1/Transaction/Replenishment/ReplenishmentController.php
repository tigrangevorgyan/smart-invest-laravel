<?php

namespace App\Http\Controllers\Api\V1\Transaction\Replenishment;

use App\Http\DataProviders\Replenishment\IndexDataProvider;
use App\Http\Requests\Transaction\Replenishment\IndexRequest;
use App\Http\Requests\Transaction\Replenishment\StoreRequest;
use App\Http\Transformers\ReplenishmentTransformer;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ReplenishmentController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        $replenishments = ReplenishmentTransformer::universal(
            $provider->getData()
        );

        return view('app.transaction.replenishment-list', [
            'replenishments' => $replenishments['items']
        ]);
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        $request->persist()->getReplenishmentData();

        return redirect()->back();
    }
}
