<?php

namespace App\Http\Controllers\Api\V1\News;

use App\Http\DataProviders\News\IndexDataProvider;
use App\Http\Requests\News\IndexRequest;
use App\Http\Transformers\NewsTransformer;
use Illuminate\View\View;

class NewsController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        $plans = NewsTransformer::universal(
            $provider->getData(),
            'detailedTransform'
        );

        return view('app.news.index', [
            'news' => $plans['items']
        ]);
    }
}
