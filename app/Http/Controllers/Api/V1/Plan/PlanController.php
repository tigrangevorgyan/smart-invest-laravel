<?php

namespace App\Http\Controllers\Api\V1\Plan;

use App\Http\DataProviders\Plan\IndexDataProvider;
use App\Http\Requests\Plan\IndexRequest;
use App\Http\Transformers\PlanTransformer;
use Illuminate\View\View;

class PlanController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        $plans = PlanTransformer::universal(
            $provider->getData(),
            'detailedTransform'
        );

        return view('app.plan.index', [
            'plans' => $plans['items']
        ]);
    }
}
