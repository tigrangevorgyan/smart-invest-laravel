<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Requests\User\ChangeTransactionCodeRequest;
use App\Http\Requests\User\UpdatePaymentSystemInfoRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserController
{
    public function update(UpdateRequest $request, User $user): User
    {
        return $request->persist()->getUser();
    }

    public function updatePaymentSystemInfo(UpdatePaymentSystemInfoRequest $request, User $user): RedirectResponse
    {
        $request->persist();

        return redirect()->back();
    }

    public function changeTransactionCode(ChangeTransactionCodeRequest $request, User $user): array
    {
        $request->persist();

        return [
            'successfully_changed' => true
        ];
    }

    public function profile(): View
    {
        return view('app.user.profile', [
            'user' => Auth::user()
        ]);
    }

    public function paymentSettings(): View
    {
        return view('app.user.payment-settings', [
            'user' => Auth::user()->load(['paymentSystemInfos', 'payeerSystemInfo'])
        ]);
    }
}
