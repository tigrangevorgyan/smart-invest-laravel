<?php

namespace App\Http\Controllers\Api\V1\CommonPage;

use App\Http\Requests\ContactUs\StoreRequest;
use Illuminate\View\View;

class CommonPageController
{
    public function aboutUs(): View
    {
        return view('app.commonPage.aboutUs');
    }

    public function faq(): View
    {
        return view('app.commonPage.faq');
    }

    public function contactUs(): View
    {
        return view('app.commonPage.contactUs');
    }

    public function privacyPolicy(): View
    {
        return view('app.commonPage.privacyPolicy');
    }

    public function storeContactUs(StoreRequest $request)
    {
        return $request->persist()->getMessage();
    }
}
