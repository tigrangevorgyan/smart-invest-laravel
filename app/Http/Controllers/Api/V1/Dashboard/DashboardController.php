<?php

namespace App\Http\Controllers\Api\V1\Dashboard;

use App\Http\DataProviders\Dashboard\OfficeDataProvider;
use App\Http\Requests\Dashboard\OfficeRequest;
use App\Http\Transformers\CollectTransformer;
use App\Http\Transformers\ReplenishmentTransformer;
use App\Http\Transformers\UserPlanTransformer;
use App\Http\Transformers\WithdrawTransformer;

class DashboardController
{
    public function office(OfficeRequest $request, OfficeDataProvider $provider)
    {
        $fullData = $provider->getData();
        $plans = UserPlanTransformer::universal($fullData['contracts']);
        $collects = CollectTransformer::universal($fullData['collects']);
        $payments = ReplenishmentTransformer::universal($fullData['payments']);
        $withdraws = WithdrawTransformer::universal($fullData['withdraws']);

        return view('app.dashboard.office', [
            'plans'     => $plans,
            'collects'  => $collects,
            'payments'  => $payments,
            'withdraws' => $withdraws
        ]);
    }
}
