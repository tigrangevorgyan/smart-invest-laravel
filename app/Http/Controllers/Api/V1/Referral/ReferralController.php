<?php

namespace App\Http\Controllers\Api\V1\Referral;

use App\Http\Requests\Referral\RulesRequest;
use Illuminate\View\View;

class ReferralController
{
    public function rules(RulesRequest $request): View
    {
        $settings = $request->getReferralAppSettings();

        return view('app.referral.rules', [
            'settings' => $settings
        ]);
    }
}
