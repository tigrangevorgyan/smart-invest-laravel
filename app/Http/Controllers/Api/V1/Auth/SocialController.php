<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use SebastianBergmann\GlobalState\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SocialController extends Controller
{
    public function redirectToGoogle(): RedirectResponse
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback(): RedirectResponse
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $existUser = User::getBySocial(User::SIGN_UP_METHODS['google'], $googleUser['id'], $googleUser['email']);
            if ($existUser) {
                Auth::login($existUser);
                return redirect(route('home-page'));
            } else {
                $newUser = User::create([
                    'first_name'    => $googleUser['given_name'],
                    'last_name'     => $googleUser['family_name'],
                    'email'         => $googleUser['email'],
                    'google_id'     => $googleUser['id'],
                    'password'      => Hash::make('asdjbjksdj*cli12387mckpodif'),
                ]);
                Auth::login($newUser);

                return redirect(route('home-page'));
            }
        } catch (Exception $e) {
            return redirect(route('login-page'))->with('status', 'Google Login failed');
        }
    }

    public function redirectToFacebook(): RedirectResponse
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(): RedirectResponse
    {
        try {
            $facebookUser = Socialite::driver('facebook')->user();
            $existUser = User::getBySocial(User::SIGN_UP_METHODS['facebook'], $facebookUser['id'],$facebookUser['email']);

            if ($existUser) {
                Auth::login($existUser);
                return redirect(route('home-page'));
            }else{
                $name = explode($facebookUser['name'], ' ');
                $count = 1;
                $newUser = User::create([
                    'first_name'    => $name[0],
                    'last_name'     => str_replace($name[0], "", $facebookUser['name'], $count),
                    'email'         => $facebookUser['email'],
                    'google_id'     => $facebookUser['id'],
                    'password'      => Hash::make('asdjbjksdj*cli12387mckpodif'),
                ]);
                Auth::login($newUser);

                return redirect(route('home-page'));
            }

        } catch (Exception $e) {
            return redirect(route('login'))->with('status', 'Facebook Login failed');
        }
    }
}
