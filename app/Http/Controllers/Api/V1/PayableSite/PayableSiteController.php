<?php

namespace App\Http\Controllers\Api\V1\PayableSite;

use App\Http\DataProviders\PayableSite\IndexDataProvider;
use App\Http\Requests\PayableSite\IndexRequest;
use App\Models\PaidContent;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PayableSiteController
{
    public function index(IndexRequest $request, IndexDataProvider $provider): View
    {
        if (Auth::user() && Auth::user()->hasAccessToPaidContent(PaidContent::TYPES['payable_sites'])) $sites = $provider->getData();

        return view('app.payableSite.index', [
            'sites'                 => $sites ?? null,
            'blockedContentInfo'    => PaidContent::findBy('type', PaidContent::TYPES['payable_sites'])
        ]);
    }
}
