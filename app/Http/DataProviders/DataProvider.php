<?php

namespace App\Http\DataProviders;

/**
 * @property int perPageCount
 */
abstract class DataProvider
{
    const PER_PAGE_COUNT = 15;

    protected $filter;
    protected $builder;
    protected $request;
    protected $withoutPagination;
    protected $model;

    protected abstract function setBuilder();

    protected function getPerPageCount()
    {
        return $withoutPagination ?? $this->request->items_per_page ?: self::PER_PAGE_COUNT;
    }

    public function getData()
    {
        if (($this->withoutPagination ?? $this->request->withoutPagination)) return $this->builder->get();

        return $this->request->withCount ?
            $this->builder->paginate($this->getPerPageCount()) :
            $this->builder->simplePaginate($this->getPerPageCount());
    }
}
