<?php

namespace App\Http\DataProviders\Withdraw;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Withdraw\IndexFilter;
use App\Models\Withdraw;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = Withdraw::filterUsing($this->filter)
            ->with('user');
    }
}
