<?php

namespace App\Http\DataProviders\Bonus;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Bonus\ReplenishmentBonusFilter;
use App\Models\ReplenishmentBonus;

class ReplenishmentBonusDataProvider extends DataProvider
{
    public function __construct(ReplenishmentBonusFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = ReplenishmentBonus::filterUsing($this->filter);
    }
}
