<?php

namespace App\Http\DataProviders\Welcome;

use App\Models\News;
use App\Models\Plan;
use Illuminate\Support\Collection;

class WelcomePageDataProvider
{
    private const POPULAR_NEWS_COUNT = 4;

    public function getData(): array
    {
        return [
            'plans' => $this->getPlans(),
            'news'  => $this->getNews()
        ];
    }

    private function getPlans(): Collection
    {
        return Plan::withCount('users')
            ->withCount('currentUsers')
            ->withCount('finishedUsers')
            ->with('logo')
            ->orderBy('users_count', 'desc')
            ->get();
    }

    private function getNews(): Collection
    {
        return News::limit(self::POPULAR_NEWS_COUNT)
            ->get();
    }
}
