<?php

namespace App\Http\DataProviders\News;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Plan\IndexFilter;
use App\Models\News;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = News::filterUsing($this->filter)
            ->with('attachment');
    }
}
