<?php

namespace App\Http\DataProviders\Replenishment;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Replenishment\IndexFilter;
use App\Models\Replenishment;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = Replenishment::filterUsing($this->filter)
            ->with('user');
    }
}
