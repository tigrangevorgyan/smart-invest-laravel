<?php

namespace App\Http\DataProviders\Plan;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Plan\IndexFilter;
use App\Models\Plan;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = Plan::filterUsing($this->filter)
            ->with('logo');
    }
}
