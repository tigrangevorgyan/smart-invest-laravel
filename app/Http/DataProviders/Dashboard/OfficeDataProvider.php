<?php

namespace App\Http\DataProviders\Dashboard;

use App\Http\Filters\Dashboard\OfficeFilter;
use App\Models\Collect;
use App\Models\Replenishment;
use App\Models\UserPlan;
use App\Models\Withdraw;
use Illuminate\Pagination\LengthAwarePaginator;

class OfficeDataProvider
{
    private $filter;
    private const LIMIT = 5;

    public function __construct(OfficeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function getData(): array
    {
        $contracts = $this->getContracts();
        $collects = $this->getCollects();
        $payments = $this->getPayments();
        $withdraws = $this->getWithdraws();

        return [
            'contracts' => $contracts,
            'collects'  => $collects,
            'payments'  => $payments,
            'withdraws' => $withdraws
        ];
    }

    private function getContracts(): LengthAwarePaginator
    {
        return UserPlan::filterUsing($this->filter)
            ->orderBy('created_at', 'desc')
            ->paginate(self::LIMIT);
    }

    private function getCollects(): LengthAwarePaginator
    {
        return Collect::filterUsing($this->filter)
            ->orderBy('created_at', 'desc')
            ->paginate(self::LIMIT);
    }

    private function getPayments(): LengthAwarePaginator
    {
        return Replenishment::filterusing($this->filter)
            ->orderBy('created_at', 'desc')
            ->paginate(self::LIMIT);
    }

    private function getWithdraws(): LengthAwarePaginator
    {
        return Withdraw::filterUsing($this->filter)
            ->orderBy('created_at', 'desc')
            ->paginate(self::LIMIT);
    }
}
