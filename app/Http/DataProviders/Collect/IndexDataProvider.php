<?php

namespace App\Http\DataProviders\Collect;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\Collect\IndexFilter;
use App\Models\Collect;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = Collect::filterUsing($this->filter)
            ->with('userPlan.user');
    }
}
