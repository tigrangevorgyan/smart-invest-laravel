<?php

namespace App\Http\DataProviders\UserPlan;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\UserPlan\MyPlansFilter;
use App\Models\UserPlan;

class MyPlansDataProvider extends DataProvider
{
    public function __construct(MyPlansFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = UserPlan::filterUsing($this->filter)
            ->with('plan.logo')
            ->orderBy('user_plans.created_at', 'desc');
    }
}
