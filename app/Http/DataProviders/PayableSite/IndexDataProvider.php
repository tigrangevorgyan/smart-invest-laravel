<?php

namespace App\Http\DataProviders\PayableSite;

use App\Http\DataProviders\DataProvider;
use App\Http\Filters\PayableSite\IndexFilter;
use App\Models\PayableSite;

class IndexDataProvider extends DataProvider
{
    public function __construct(IndexFilter $filter)
    {
        $this->filter = $filter;
        $this->request = $filter->request;

        $this->setBuilder();
    }

    protected function setBuilder()
    {
        $this->builder = PayableSite::filterUsing($this->filter);
    }
}
