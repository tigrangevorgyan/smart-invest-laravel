<?php

namespace App\Http\Filters\Withdraw;

use App\Http\Filters\SorterFilter;
use App\Http\Requests\Transaction\Withdraw\IndexRequest;

class IndexFilter extends SorterFilter
{
    function __construct(IndexRequest $request)
    {
        $this->setRequest($request);
    }

    public function rules(): array
    {
        return [
            //
        ];
    }

    protected function orders(): array
    {
        return [
            //
        ];
    }
}
