<?php

namespace App\Http\Filters\Dashboard;

use App\Http\Filters\AbstractFilter;
use App\Http\Requests\Dashboard\OfficeRequest;

class OfficeFilter extends AbstractFilter
{
    function __construct(OfficeRequest $request)
    {
        $this->setRequest($request);
    }

    public function rules(): array
    {
        return [
            //
        ];
    }
}
