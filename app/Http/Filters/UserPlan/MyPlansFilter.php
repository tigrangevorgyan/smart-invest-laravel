<?php

namespace App\Http\Filters\UserPlan;

use App\Http\Filters\AbstractFilter;
use App\Http\Requests\UserPlan\MyPlansRequest;

class MyPlansFilter extends AbstractFilter
{
    function __construct(MyPlansRequest $request)
    {
        $this->setRequest($request);
    }

    public function rules(): array
    {
        return [
            //
        ];
    }
}
