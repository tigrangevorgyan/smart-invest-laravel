<?php

namespace App\Http\Filters\Bonus;

use App\Http\Filters\SorterFilter;
use App\Http\Requests\Transaction\Bonus\ReplenishmentBonusRequest;

class ReplenishmentBonusFilter extends SorterFilter
{
    function __construct(ReplenishmentBonusRequest $request)
    {
        $this->setRequest($request);
    }

    public function rules(): array
    {
        return [
            //
        ];
    }

    protected function orders(): array
    {
        return [
            //
        ];
    }
}
