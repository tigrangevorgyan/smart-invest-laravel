<?php

namespace App\Policies;

use App\Models\Plan;
use App\Models\User;

class PlanPolicy extends BasePolicy
{
    protected $acceptableForAllUsers = [
        'subscribe'
    ];

    public function subscribe(User $user, Plan $plan): bool
    {
        return $user->canSubscribe($plan);
    }
}
