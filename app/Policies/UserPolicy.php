<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends BasePolicy
{
    protected $acceptableForAllUsers = [
        'update'
    ];

    public function update(User $user): bool
    {
        return $user->isAuthUser();
    }
}
