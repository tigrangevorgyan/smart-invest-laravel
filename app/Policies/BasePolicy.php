<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    protected const PERMISSION_FOR_ACTION = [];

    protected $acceptableForAllUsers = [];

    public function before(User $authUser, string $methodName): ?bool
    {
        if (in_array($methodName, $this->acceptableForAllUsers)) return null;

        if ($authUser->isAdmin()) return null;

        return false;
    }
}
