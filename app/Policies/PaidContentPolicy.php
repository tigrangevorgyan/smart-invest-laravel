<?php

namespace App\Policies;

use App\Models\PaidContent;
use App\Models\User;

class PaidContentPolicy extends BasePolicy
{
    protected $acceptableForAllUsers = [
        'unblock'
    ];

    public function unblock(User $user, PaidContent $paidContent): bool
    {
        return $user->canUnblock($paidContent);
    }
}
