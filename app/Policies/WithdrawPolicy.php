<?php

namespace App\Policies;

use App\Models\User;

class WithdrawPolicy extends BasePolicy
{
    protected $acceptableForAllUsers = [
        'store'
    ];

    public function store(User $user, float $amount): bool
    {
        return $user->canWithdraw($amount);
    }
}
