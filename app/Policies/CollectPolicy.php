<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserPlan;

class CollectPolicy extends BasePolicy
{
    protected $acceptableForAllUsers = [
        'store'
    ];

    public function store(User $user, UserPlan $userPlan): bool
    {
        return $userPlan->canCollect();
    }
}
